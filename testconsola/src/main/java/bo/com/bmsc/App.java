package bo.com.bmsc;

import lombok.Getter;

/**
 * Hello world!
 *
 */
@Getter
public class App
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
