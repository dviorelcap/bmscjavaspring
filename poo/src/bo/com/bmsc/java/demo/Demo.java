package bo.com.bmsc.java.demo;

public class Demo {

    static void mostrarAutomovil(Automovil automovil){
        System.out.println("Encendido: " + automovil.estaEncendido() + ", velocidad " + automovil.obtenerVelocidad());
    }
    public static void main(String[] args) {
        Automovil automovil = new Automovil();
        Automovil.probar();
        mostrarAutomovil(automovil);
        automovil.encender();
        automovil.acelerar(10);
        mostrarAutomovil(automovil);
        automovil.desacelerar(50);
        mostrarAutomovil(automovil);
        automovil.apagar();
        //automovil.velocidad = 0;
        mostrarAutomovil(automovil);
    }

//    static void mostrarAutomovil(Automovil automovil){
//        System.out.println("Encendido: " + automovil.encendido + ", velocidad " + automovil.velocidad);
//    }
//    public static void main(String[] args) {
//        Automovil automovil = new Automovil();
//        mostrarAutomovil(automovil);
//        automovil.encendido = true;
//        automovil.velocidad = 10;
//        mostrarAutomovil(automovil);
//        automovil.velocidad = -5;
//        mostrarAutomovil(automovil);
//        automovil.encendido = false;
//        //automovil.velocidad = 0;
//        mostrarAutomovil(automovil);
//    }

}
