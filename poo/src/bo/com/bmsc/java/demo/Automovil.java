package bo.com.bmsc.java.demo;

public class Automovil {

    private boolean encendido;
    private int velocidad;

    public boolean estaEncendido(){
        return encendido;
    }
    public int obtenerVelocidad(){
        return velocidad;
    }
    public static void probar(){

    }
    public void encender(){
        encendido = true;
    }
    public void apagar(){
        encendido = false;
        velocidad = 0;
    }
    public void acelerar(int aceleracion){
        velocidad = velocidad + aceleracion;
    }
    public void desacelerar(int desaceleracion){
        if (desaceleracion > velocidad){
            // throw new RuntimeException("No se puede desacelerar tanto");
            desaceleracion = velocidad;
        }
        velocidad = velocidad - desaceleracion;
    }

    public boolean isEncendido() {
        return encendido;
    }

    public void setEncendido(boolean encendido) {
        this.encendido = encendido;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }
}
