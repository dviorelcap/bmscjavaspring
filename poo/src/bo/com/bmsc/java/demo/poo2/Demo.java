package bo.com.bmsc.java.demo.poo2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;


public class Demo {
    public static void main(String[] args) {
//        Persona persona = new Alumno("nombrealumno", "apellidoalumno", "123");
//        System.out.println(persona.mostrarDatos());
//        List<String> cadenas = new ArrayList<>();
//        List<String> cadenas2 = new LinkedList<>();
        // cadenas2.get(3);
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Ingrese su nombre: ");
//        String nombre = scanner.next();
//        System.out.println("Hola " + nombre);
//        System.err.println("Error con el nombre " + nombre);
        // System.exit(-1);
//        int valor = 3;
//        Integer valorWrapper = 3;
//        valorWrapper.toString();
//        valorWrapper.longValue();
        int a = 1;
        int b = 1;
        System.out.println("Son iguales: " + (a == b));
        Profesor p1 = new Profesor("np", "ap");
        Profesor p2 = new Profesor("np ", "ap");
        System.out.println("Son iguales (referencia): " + (p1 == p2));
        System.out.println("Son iguales (valores): " + (p1.equals(p2)));
    }
}
