package bo.com.bmsc.java.demo.poo2;

public class Profesor extends Persona {

    public Profesor(String nombre, String apellido) {
        super(nombre, apellido);
    }

    @Override
    String mostrarDatos() {
        return "datos de profesor";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if (!(obj instanceof Profesor)){
            return false;
        }
        Profesor profesor = (Profesor) obj;
        return this.nombre.trim().equals(profesor.nombre.trim()) && this.apellido.equals(profesor.apellido);
    }
}
