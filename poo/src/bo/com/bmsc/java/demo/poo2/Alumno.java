package bo.com.bmsc.java.demo.poo2;

public class Alumno extends Persona {

    public Alumno(String nombre, String apellido, String numeroRegistro) {
        super(nombre, apellido);
        this.numeroRegistro = numeroRegistro;
    }

    private String numeroRegistro;

    public String getNumeroRegistro() {
        System.out.println(this.getNombre());
        System.out.println(this.getApellido());
        System.out.println(this.nombre);
        return numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    @Override
    String mostrarDatos() {
        return "datos de Alumno";
    }

    @Override
    public String toString() {
        return "Alumno{" +
                "numeroRegistro='" + numeroRegistro + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                '}';
    }
}
