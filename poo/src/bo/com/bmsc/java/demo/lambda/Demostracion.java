package bo.com.bmsc.java.demo.lambda;

import java.util.function.Function;
import java.util.function.Predicate;

public class Demostracion {

    private static boolean esPar(int x){
        return x % 2 ==0;
    }

    public static void main(String[] args) {
        int n = -6;
        // System.out.println(esPar(n));
        Predicate<Integer> esPar = x -> x % 2 ==0;
        // System.out.println(esPar.test(n));
        // System.out.println(aplicar(n, esPar));
        System.out.println(aplicar(n, x -> x > 0));
        Function<Integer, Integer> duplicar = (numero) -> { return numero * 2;};
        System.out.println(duplicar.apply(3));
    }

    private static boolean aplicar(int n, Predicate<Integer> funcion){
        // logica
        return funcion.test(n);
    }


}
