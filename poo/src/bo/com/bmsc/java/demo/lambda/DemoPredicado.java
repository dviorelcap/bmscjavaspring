package bo.com.bmsc.java.demo.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DemoPredicado {

    public static void main(String[] args) {
        List<Persona> personas = new ArrayList<>();
        personas.add(new Persona(1, "nombre 1", "apellido 1"));
        personas.add(new Persona(2, "nombre 2", "apellido 2"));
        personas.add(new Persona(3, "nombre 3", "apellido 3"));
        personas.add(new Persona(5, "Juan", "Perez"));
        personas.add(new Persona(6, "Javier", "Portillo"));

//        // estilo antiguo
//        List<Persona> personasMatch = new ArrayList<>();
//        for (Persona persona: personas){
//            if (persona.getNombre().startsWith("J")){
//                personasMatch.add(persona);
//            }
//        }
//        // Mostramos el resultado
//        for (Persona persona: personasMatch){
//            System.out.println(persona);
//        }

//        List<Persona> personasMatch = personas.stream().filter(p -> p.getNombre().startsWith("J"))
//                .collect(Collectors.toList());
//        personasMatch.forEach(System.out::println);

        personas.stream().filter(p -> p.getNombre().startsWith("J")).forEach(System.out::println);
        System.out.println(IntStream.range(1, 5).reduce((a, b) -> a + b).getAsInt());
        // List<String> nombres = personas.stream().map(p -> p.getNombre()).collect(Collectors.toList());
        List<String> nombres = personas.stream().map(Persona::getNombre).collect(Collectors.toList());
        nombres.forEach(System.out::println);
    }

}
