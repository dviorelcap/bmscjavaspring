package bo.com.bmsc.java.demo.excepciones;

public class MiExcepcionPersonalizada extends RuntimeException {

    private int codigoError;

    public int getCodigoError() {
        return codigoError;
    }

    public MiExcepcionPersonalizada(String message, int codigoError) {
        super(message);
        this.codigoError = codigoError;
    }

    public MiExcepcionPersonalizada(String message, Throwable cause, int codigoError) {
        super(message, cause);
        this.codigoError = codigoError;
    }
}
