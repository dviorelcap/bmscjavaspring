package bo.com.bmsc.java.demo.excepciones;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Demo {

    public static void main(String[] args) {
        try {
            posibleDivisionEntreCero(4, 0);
            escribirAArchivo(Arrays.asList("uno", "dos"), "I:/javaspring/archivo.txt");
        }
//        catch (ArrayIndexOutOfBoundsException e){
//            System.out.println("Hubo un error relacionado con arrays");
//        }
//        catch (IOException e){
//            System.out.println("Hubo un error relacionado con archivos");
//        }
        catch (MiExcepcionPersonalizada e){
            System.out.println("Hubo un error con codigo " + e.getCodigoError() + " y mensaje " + e.getMessage());
        }
        catch (Exception e){
            System.out.println("Hubo un error");
        }

    }

    private static void posibleDivisionEntreCero(int a, int b){
        try {
            System.out.println(a / b);
        }
        catch (Exception e){
            throw new MiExcepcionPersonalizada("Error al dividir", 1);
        }
    }

    private static void escribirAArchivo(List<String> textos, String rutaArchivo) throws IOException {
        try {
            Files.write(Paths.get(rutaArchivo), textos);
        }
        catch (Exception e){
            throw new MiExcepcionPersonalizada("Error con los archivos", 2);
        }
    }

}
