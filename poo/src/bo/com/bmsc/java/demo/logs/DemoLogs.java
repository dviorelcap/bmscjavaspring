package bo.com.bmsc.java.demo.logs;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class DemoLogs {

    private static Logger log = LogManager.getLogger(DemoLogs.class);

    public static void main(String[] args) {
        log.trace("log trace");
        log.debug("log debug");
        log.info("log info");
        log.warn("log warn");
        log.error("log error");
        log.fatal("log fatal");
    }

}
