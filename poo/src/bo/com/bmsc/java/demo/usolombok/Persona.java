package bo.com.bmsc.java.demo.usolombok;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@ToString
@EqualsAndHashCode
//@Data
@NoArgsConstructor
@AllArgsConstructor
public class Persona {

    private Integer ci;
    private String nombre;
    @Getter(AccessLevel.NONE)
    private String apellido;
    private LocalDate fechaNacimiento;

}
