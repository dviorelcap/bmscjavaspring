package bo.com.bmsc.java.demo.usolombok;

import java.time.LocalDate;
import java.time.Month;

public class DemoLombok {

    public static void main(String[] args) {
         Persona persona = new Persona(1, "nombre 1", "apellido 1",
                 LocalDate.of(2000, Month.DECEMBER, 25));
//        Persona persona = new Persona();
        System.out.println(persona);
        System.out.println(persona.getNombre());
    }

}
