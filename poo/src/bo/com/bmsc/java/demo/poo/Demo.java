package bo.com.bmsc.java.demo.poo;

import bo.com.bmsc.java.demo.Automovil;

public class Demo {
    public static void main(String[] args) {
        Persona persona = new Persona("nombrepersona", "apellidopersona");
        System.out.println("Persona 1: " + persona);
        Alumno alumno = new Alumno("nombrealumno", "apellidoalumno", "123alumno");
        // persona = alumno;
        System.out.println(alumno);
        System.out.println("Persona 2: " + persona);
        Persona persona2 = new Alumno("nombrealumno2", "apellidoalumno2", "123alumno2");
        // Persona persona3 = new Automovil();
        // persona2 = persona;
        Alumno alumno2;
        if (persona2 instanceof Alumno) {
            System.out.println("puede asignar");
            alumno2 = (Alumno) persona2;
        }
        else{
            System.out.println("no puede asignar");
            alumno2 = null;
        }
        System.out.println("Alumno 2 basado en persona 2 es " + alumno2);
    }
}
