package bo.com.bmsc.java.demo.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionOracle {

    private static ConexionOracle instancia = new ConexionOracle();

    public static ConexionOracle getInstancia(){
        return instancia;
    }

    public Connection obtenerConexion() throws ClassNotFoundException, SQLException {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        String urlBd = "jdbc:oracle:thin:@//localhost/xe";
        String user = "SYSTEM";
        String password = "adminadmin";
        return DriverManager.getConnection(urlBd, user, password);
    }


}
