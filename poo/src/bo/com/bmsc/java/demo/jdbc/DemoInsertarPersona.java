package bo.com.bmsc.java.demo.jdbc;

import bo.com.bmsc.java.demo.lambda.Persona;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class DemoInsertarPersona {

    private static Logger log = LogManager.getLogger(DemoInsertarPersona.class);

    // public void insertarPersona(Integer ci, String nombre, String apellido){
    public void insertarPersona(Persona persona){
        Connection conexion = null;
        try {
            conexion = ConexionOracle.getInstancia().obtenerConexion();
            PreparedStatement ps = conexion.prepareStatement(
                    "INSERT INTO PERSONA VALUES(?, ?, ?)");
            ps.setInt(1, persona.getCi());
            ps.setString(2, persona.getNombre());
            ps.setString(3, persona.getApellido());
            ps.executeUpdate();
        } catch (Exception e) {
            log.error("Algo salio mal ", e);
        }
    }

}
