package bo.com.bmsc.java.demo.jdbc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class DemoJdbc {

    private static Logger log = LogManager.getLogger(DemoJdbc.class);

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Connection conexion;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String urlBd = "jdbc:oracle:thin:@//localhost/xe";
            String user = "SYSTEM";
            String password = "adminadmin";
            conexion = DriverManager.getConnection(urlBd, user, password);
            Statement statement = conexion.createStatement();
            ResultSet resultados = statement.executeQuery("SELECT * FROM PERSONA");
            while(resultados.next()){
                log.info(resultados.getInt(1) + " " + resultados.getString(2)
                    + " " + resultados.getString(3));
            }
        } catch (Exception e) {
            log.error("Algo salio mal ", e);
        }

    }

}
