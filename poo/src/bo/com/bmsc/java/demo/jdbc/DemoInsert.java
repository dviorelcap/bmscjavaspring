package bo.com.bmsc.java.demo.jdbc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Scanner;

public class DemoInsert {
    private static Logger log = LogManager.getLogger(DemoInsert.class);

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        try {
            Connection conexion = ConexionOracle.getInstancia().obtenerConexion();
            Scanner scanner = new Scanner(System.in);
            System.out.println("Ingrese CI: ");
            Integer ci = scanner.nextInt();
            System.out.println("Ingrese nombre: ");
            String nombre = scanner.next();
            System.out.println("Ingrese apellido: ");
            String apellido = scanner.next();
            PreparedStatement ps = conexion.prepareStatement(
                    "INSERT INTO PERSONA VALUES(?, ?, ?)");
            ps.setInt(1, ci);
            ps.setString(2, nombre);
            ps.setString(3, apellido);
            ps.executeUpdate();
        } catch (Exception e) {
            log.error("Algo salio mal ", e);
        }

    }
}
