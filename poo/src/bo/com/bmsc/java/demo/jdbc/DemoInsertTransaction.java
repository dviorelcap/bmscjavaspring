package bo.com.bmsc.java.demo.jdbc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class DemoInsertTransaction {
    private static Logger log = LogManager.getLogger(DemoInsertTransaction.class);

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Connection conexion = null;
        try {
            conexion = ConexionOracle.getInstancia().obtenerConexion();
            conexion.setAutoCommit(false);
            Scanner scanner = new Scanner(System.in);
            PreparedStatement ps = conexion.prepareStatement(
                    "INSERT INTO PERSONA VALUES(?, ?, ?)");
            ps.setInt(1, 9);
            ps.setString(2, "nombre 9");
            ps.setString(3, "apellido 9");
            ps.executeUpdate();
            ps = conexion.prepareStatement(
                    "INSERT INTO PERSONA VALUES(?, ?, ?)");
            ps.setInt(1, 14);
            ps.setString(2, "nombre 4");
            ps.setString(3, "apellido 4");
            ps.executeUpdate();
            conexion.commit();
        } catch (Exception e) {
            if (conexion != null){
                conexion.rollback();
            }
            log.error("Algo salio mal ", e);
        }

    }
}
