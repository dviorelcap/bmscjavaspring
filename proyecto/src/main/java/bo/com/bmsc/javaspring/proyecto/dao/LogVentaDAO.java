package bo.com.bmsc.javaspring.proyecto.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bo.com.bmsc.javaspring.proyecto.entidades.Cliente;
import bo.com.bmsc.javaspring.proyecto.entidades.LogVenta;

@Repository
public interface LogVentaDAO extends JpaRepository<LogVenta, Integer> {

}
