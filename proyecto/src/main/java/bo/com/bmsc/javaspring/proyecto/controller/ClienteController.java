package bo.com.bmsc.javaspring.proyecto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import bo.com.bmsc.javaspring.proyecto.dto.ClienteDTO;
import bo.com.bmsc.javaspring.proyecto.entidades.Cliente;
import bo.com.bmsc.javaspring.proyecto.mapper.MapStructMapper;
import bo.com.bmsc.javaspring.proyecto.services.ClienteService;

@RestController
@RequestMapping("/rest/cliente")
public class ClienteController {

	@Autowired
	ClienteService clienteService;
	
//	@RequestMapping(method = RequestMethod.GET, 
//			produces = {MediaType.APPLICATION_JSON_VALUE
//					//, MediaType.APPLICATION_XML_VALUE
//					})
//	public List<Cliente> listarClientes(){
//		return clienteService.listar();
//	}
	
	@RequestMapping(method = RequestMethod.GET, 
			produces = {MediaType.APPLICATION_JSON_VALUE
					//, MediaType.APPLICATION_XML_VALUE
					})
	public List<ClienteDTO> listarClientes(){
		return mapStructMapper.mapClientesToClientesDTO(clienteService.listar());
	}
	
//	@RequestMapping(value = "/{id}", method = RequestMethod.GET, 
//			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
//	public Cliente obtenerCliente(@PathVariable("id") Integer id){
//		return clienteService.obtener(id);
//	}
	@Autowired
	MapStructMapper mapStructMapper;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, 
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ClienteDTO obtenerCliente(@PathVariable("id") Integer id){
		Cliente cliente = clienteService.obtener(id);
		return mapStructMapper.mapClienteToClienteDTO(cliente);
	}
	
	@RequestMapping(method = RequestMethod.POST, 
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ClienteDTO crearCliente(@RequestBody ClienteDTO clienteDTO) {
		Cliente cliente = mapStructMapper.mapClienteDTOToCliente(clienteDTO);
		clienteService.guardar(cliente);
		return mapStructMapper.mapClienteToClienteDTO(cliente);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void borrarCliente(@PathVariable("id") Integer id){
		clienteService.borrar(id);		
	}
	
	
}
