package bo.com.bmsc.javaspring.proyecto.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import bo.com.bmsc.javaspring.proyecto.dto.ClienteDTO;
import bo.com.bmsc.javaspring.proyecto.entidades.Cliente;

@Mapper(componentModel = "spring")
public interface MapStructMapper {
	
	Cliente mapClienteDTOToCliente(ClienteDTO clienteDTO);

	Cliente mapClienteDTOToCliente(bo.com.bmsc.javaspring.proyecto.soap.ws.ClienteDTO clienteDTO);
	
	// @Mapping(source = "nombre", target = "name")
	ClienteDTO mapClienteToClienteDTO(Cliente cliente);
	bo.com.bmsc.javaspring.proyecto.soap.ws.ClienteDTO mapClienteToClienteSoapDTO(Cliente cliente);

	List<ClienteDTO> mapClientesToClientesDTO(List<Cliente> cliente);
	// List<bo.com.bmsc.javaspring.proyecto.soap.ws.ClienteDTO> mapClientesToClientesSoapDTO(List<Cliente> cliente);
	
}
