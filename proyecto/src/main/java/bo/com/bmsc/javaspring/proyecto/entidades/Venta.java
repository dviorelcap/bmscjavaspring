package bo.com.bmsc.javaspring.proyecto.entidades;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CascadeType;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Venta extends EntidadBase {

	@Id
	@GeneratedValue
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "ID_CLIENTE", nullable = false)
	private Cliente cliente;
	
	
	private LocalDateTime fecha;
	
	@Column(precision = 10, scale=2)
	private BigDecimal precioTotal;
	
	// @OneToMany(mappedBy = "venta", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
	@OneToMany(mappedBy = "venta", fetch = FetchType.LAZY)
	private Set<VentaDetalle> detalles = new HashSet<>();
	
	
}
