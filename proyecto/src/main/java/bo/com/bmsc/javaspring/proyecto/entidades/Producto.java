package bo.com.bmsc.javaspring.proyecto.entidades;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Table(name = "PROY_PRODUCTO")
@ToString
public class Producto extends EntidadBase{

	@Id
	@GeneratedValue
	Integer id;
	
	@Column(name="codigo", length = 15, unique = true, nullable = false)
	String codigo;
	
	@Column(name="nombre", length = 50, unique = true, nullable = false)
	String nombre;
	
	@Column(name = "precio", precision = 10, scale = 2)
	BigDecimal precio;
	
	@Column(name="stock", nullable = false)
	Integer stock;
	
//	//@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="fecha_alta", nullable = false)
//	LocalDateTime fechaAlta;
//
////	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="fecha_baja")
//	LocalDateTime fechaBaja;
//
//	//@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="fecha_ultima_modificacion", nullable = false)
//	LocalDateTime fechaUltimaModificacion;
	
}
