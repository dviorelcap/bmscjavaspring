package bo.com.bmsc.javaspring.proyecto.xmladapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class LocalDateAdapter extends XmlAdapter<Date, LocalDate> {
    @Override
    public LocalDate unmarshal(Date date) throws Exception {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    @Override
    public Date marshal(LocalDate localDate) throws Exception {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
}
