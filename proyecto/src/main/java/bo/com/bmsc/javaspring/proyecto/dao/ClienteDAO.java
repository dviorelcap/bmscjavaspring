package bo.com.bmsc.javaspring.proyecto.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import bo.com.bmsc.javaspring.proyecto.entidades.Cliente;

@Repository
public interface ClienteDAO extends JpaRepository<Cliente, Integer> {

	public Cliente findByCi(String ci);

	@Query("select c from Cliente c where c.fechaBaja is null")
	public List<Cliente> listarTodos();
	
}
