package bo.com.bmsc.javaspring.proyecto.soap;

import bo.com.bmsc.javaspring.proyecto.entidades.Cliente;
import bo.com.bmsc.javaspring.proyecto.mapper.MapStructMapper;
import bo.com.bmsc.javaspring.proyecto.services.ClienteService;
import bo.com.bmsc.javaspring.proyecto.soap.ws.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

@Endpoint
public class WebServiceEndpoint {

    private static final String NAMESPACE = "http://bmsc.com.bo/javaspring/proyecto/soap/ws";

    @Autowired
    ClienteService clienteService;

    @Autowired
    MapStructMapper mapStructMapper;

    @PayloadRoot(namespace = NAMESPACE, localPart = "crearClientesRequest")
    @ResponsePayload
    public CrearClientesResponse crearCliente(@RequestPayload CrearClientesRequest peticion){
        ClienteDTO clienteDTO = peticion.getCliente();
        Cliente cliente = mapStructMapper.mapClienteDTOToCliente(clienteDTO);
        clienteService.guardar(cliente);
        CrearClientesResponse respuesta = new CrearClientesResponse();
        respuesta.setCliente(mapStructMapper.mapClienteToClienteSoapDTO(cliente));
        return respuesta;
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "listarClientesRequest")
    @ResponsePayload
    public ListarClientesResponse listarClientes(@RequestPayload ListarClientesRequest peticion){
        String filtro = peticion.getFiltro();
        List<Cliente> clientes = clienteService.listar();

        ListarClientesResponse respuesta = new ListarClientesResponse();
        clientes.forEach(c -> respuesta.getClientes().add(mapStructMapper.mapClienteToClienteSoapDTO(c)));
        return respuesta;
    }

}
