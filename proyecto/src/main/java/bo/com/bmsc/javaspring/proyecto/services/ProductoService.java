package bo.com.bmsc.javaspring.proyecto.services;

import bo.com.bmsc.javaspring.proyecto.dao.ClienteDAO;
import bo.com.bmsc.javaspring.proyecto.dao.ProductoDAO;
import bo.com.bmsc.javaspring.proyecto.entidades.Cliente;
import bo.com.bmsc.javaspring.proyecto.entidades.Producto;
import bo.com.bmsc.javaspring.proyecto.excepciones.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductoService extends BaseService<Producto> {

	@Autowired
	ProductoDAO productoDAO;

	@Override
	void validarAlta(Producto producto) {
		Producto productoExiste = productoDAO.findByCodigo(producto.getCodigo());
		if(productoExiste != null && !productoExiste.getId().equals(producto.getId())) {
			throw new CustomException("Ya existe un producto con el Codigo " + producto.getCodigo());
		}
		productoExiste = productoDAO.findByNombre(producto.getNombre());
		if(productoExiste != null && !productoExiste.getId().equals(producto.getId())) {
			throw new CustomException("Ya existe un producto con el Nombre " + producto.getCodigo());
		}
	}

	@Override
	void validarModificacion(Producto productoAntiguo, Producto productoNuevo) {
		if (!productoAntiguo.getCodigo().equals(productoNuevo.getCodigo())){
			throw new CustomException("No se permite la modificación del código del producto");
		}
	}

	@Override
	void validarBaja(Producto entidad) {

	}

	@Override
	JpaRepository<Producto, Integer> getDao() {
		return productoDAO;
	}

//	Logger logger= LoggerFactory.getLogger(ProductoService.class);
//
//	@Autowired
//	ProductoDAO productoDAO;
//
//	public Producto guardar(Producto producto){
//		// Log
//		logger.debug("Intenta guardar producto " + producto);
//		// Validaciones
//		Producto productoExiste = productoDAO.findByCodigo(producto.getCodigo());
//		if(productoExiste != null && !productoExiste.getId().equals(producto.getId())) {
//			throw new RuntimeException("Ya existe un producto con el Codigo " + producto.getCodigo());
//		}
//		productoExiste = productoDAO.findByNombre(producto.getNombre());
//		if(productoExiste != null && !productoExiste.getId().equals(producto.getId())) {
//			throw new RuntimeException("Ya existe un producto con el Nombre " + producto.getCodigo());
//		}
//		if (producto.getCodigo().length() > 15){
//			throw new RuntimeException("El codigo del producto, no debe exceder los 15 caracateres");
//		}
//		// Ejecucion de la operacion
//		if (producto.getId() == null) {
//			producto.setFechaAlta(LocalDateTime.now());
//		}
//		else{
//			producto.setFechaAlta(productoDAO.getById(producto.getId()).getFechaAlta());
//		}
//		producto.setFechaUltimaModificacion(LocalDateTime.now());
//		productoDAO.saveAndFlush(producto);
//		// log de salida
//		logger.debug("Guardó el producto " + producto);
//		return producto;
//	}
//
//	public void borrar(Integer id){
//		logger.debug("Intenta borrar cliente con id " + id);
//		Producto producto = productoDAO.getById(id);
//		if (producto == null){
//			throw new RuntimeException("No se encontró producto con ID " + id);
//		}
//		if (producto.getFechaBaja() != null){
//			throw new RuntimeException("El producto con ID " + id + " ya se encontraba eliminado");
//		}
//		// otras validaciones...
//
//		producto.setFechaBaja(LocalDateTime.now());
//		productoDAO.saveAndFlush(producto);
//		logger.debug("Eliminó el producto " + producto);
//	}
//
//	public Producto obtener(Integer id){
//		Producto producto = productoDAO.getById(id);
//		if(producto != null && producto.getFechaBaja() != null) {
//			return null;
//		}
//		return producto;
//	}
//
//	//public List<Cliente> listar(){
////		return clienteDAO.findAll().stream()
//	//			.filter(c -> c.getFechaBaja() == null).toList();
//	//}
//
//	public List<Producto> listar(){
//		return productoDAO.findAll().stream().filter(p -> p.getFechaBaja() == null)
//				.collect(Collectors.toList());
//	}
	
}
