package bo.com.bmsc.javaspring.proyecto.entidades;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.LocalDateTime;

@MappedSuperclass
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public abstract class EntidadBase {

    public abstract Integer getId();

    @Column(name="fecha_alta", nullable = false)
    protected LocalDateTime fechaAlta;

    //	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="fecha_baja")
    protected LocalDateTime fechaBaja;

    //@Temporal(TemporalType.TIMESTAMP)
    @Column(name="fecha_ultima_modificacion", nullable = false)
    protected LocalDateTime fechaUltimaModificacion;

}
