package bo.com.bmsc.javaspring.proyecto.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bo.com.bmsc.javaspring.proyecto.entidades.Cliente;
import bo.com.bmsc.javaspring.proyecto.entidades.Producto;

@Repository
public interface ProductoDAO extends JpaRepository<Producto, Integer> {

    Producto findByCodigo(String codigo);

    Producto findByNombre(String nombre);

}
