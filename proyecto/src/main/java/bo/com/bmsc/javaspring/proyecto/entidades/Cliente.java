package bo.com.bmsc.javaspring.proyecto.entidades;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@Table(name = "PROY_CLIENTE")
public class Cliente extends EntidadBase {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CLIENTE")
	Integer id;
	
	String nombre;
	
	String apellido;
	
	@Column(length = 20, unique = true)
	String ci;
	
	String codigo;
	
	LocalDate fechaNacimiento;
	
//	LocalDateTime fechaAlta;
//
//	LocalDateTime fechaBaja;
//
//	LocalDateTime fechaUltimaModificacion;
	
}
