package bo.com.bmsc.javaspring.proyecto.entidades;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
public class VentaDetalle extends EntidadBase {

	@Id
	@GeneratedValue
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "ID_VENTA", nullable = false)
	private Venta venta;
	
	@ManyToOne
	@JoinColumn(name = "ID_PRODUCTO")
	private Producto producto;
	
	Integer cantidad;
	
	@Column(precision = 10, scale = 2)
	BigDecimal precioUnitario;
	
	@Column(precision = 10, scale = 2)
	BigDecimal precioSubTotal;
	
}
