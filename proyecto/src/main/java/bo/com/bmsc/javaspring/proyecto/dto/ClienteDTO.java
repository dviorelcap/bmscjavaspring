package bo.com.bmsc.javaspring.proyecto.dto;

import java.time.LocalDate;

import bo.com.bmsc.javaspring.proyecto.xmladapter.LocalDateAdapter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Getter
@Setter
@ToString
@XmlRootElement
public class ClienteDTO {
	@XmlElement
	Integer id;
	@XmlElement
	String nombre;
	@XmlElement
	String apellido;
	@XmlElement
	String ci;
	@XmlElement
	String codigo;
	@XmlElement
	@XmlJavaTypeAdapter(LocalDateAdapter.class)
	LocalDate fechaNacimiento;
	
}
