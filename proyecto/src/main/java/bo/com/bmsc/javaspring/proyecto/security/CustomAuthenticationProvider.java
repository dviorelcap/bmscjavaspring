package bo.com.bmsc.javaspring.proyecto.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

//    @Autowired
//    UsuarioService usuarioService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();
        // usuarioService.authenticar(usename, password);
        if (username.equals("admin") && password.equals("adminadmin")) {
            return new UsernamePasswordAuthenticationToken(
                    username, password, Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN")));
        }
        throw new RuntimeException("Credenciales no validas");
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
