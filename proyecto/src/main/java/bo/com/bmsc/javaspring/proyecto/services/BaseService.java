package bo.com.bmsc.javaspring.proyecto.services;

import bo.com.bmsc.javaspring.proyecto.entidades.Cliente;
import bo.com.bmsc.javaspring.proyecto.entidades.EntidadBase;
import bo.com.bmsc.javaspring.proyecto.excepciones.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseService<T extends EntidadBase> {

    Logger logger= LoggerFactory.getLogger(this.getClass());

    abstract void validarAlta(T entidad);
    abstract void validarModificacion(T entidadAntigua, T entidadNueva);
    abstract void validarBaja(T entidad);
    abstract JpaRepository<T, Integer> getDao();

    public T guardar(T entidad){
        logger.debug("Intenta guardar " + entidad);
        if (entidad.getId() == null){
            validarAlta(entidad);
        }
        else{
            T entidadAntigua = getDao().getById(entidad.getId());
            validarModificacion(entidadAntigua, entidad);
        }
        if (entidad.getId() == null){
            entidad.setFechaAlta(LocalDateTime.now());
        }
        entidad.setFechaUltimaModificacion(LocalDateTime.now());
        getDao().saveAndFlush(entidad);
        logger.debug("Guardó " + entidad);
        return entidad;
    }

    public void borrar(Integer id){
        logger.debug("Intenta borrar item con id " + id);
        T entidad = getDao().getById(id);
        if (entidad == null){
            throw new CustomException("No se encontró objeto con ID " + id);
        }
        validarBaja(entidad);
        entidad.setFechaBaja(LocalDateTime.now());
        getDao().saveAndFlush(entidad);
        logger.debug("Eliminó la entidad " + entidad);
    }

    public T obtener(Integer id){
        T entidad = getDao().getById(id);
        if(entidad == null || entidad.getFechaBaja() != null) {
            return null;
        }
        return entidad;
    }

    public List<T> listar(){
        return getDao().findAll().stream().filter(e -> e.getFechaBaja() == null)
                .collect(Collectors.toList());
    }


}
