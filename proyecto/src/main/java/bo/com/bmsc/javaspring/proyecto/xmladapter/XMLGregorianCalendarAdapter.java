package bo.com.bmsc.javaspring.proyecto.xmladapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.GregorianCalendar;

public class XMLGregorianCalendarAdapter extends XmlAdapter<Date, XMLGregorianCalendar> {

    @Override
    public XMLGregorianCalendar unmarshal(Date date) throws Exception {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
    }

    @Override
    public Date marshal(XMLGregorianCalendar xmlGregorianCalendar) throws Exception {
        return xmlGregorianCalendar.toGregorianCalendar().getTime();
    }
}
