package bo.com.bmsc.javaspring.proyecto.controller;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import bo.com.bmsc.javaspring.proyecto.entidades.Producto;
import bo.com.bmsc.javaspring.proyecto.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bo.com.bmsc.javaspring.proyecto.entidades.Cliente;
import bo.com.bmsc.javaspring.proyecto.services.ClienteService;

@RestController
public class DemoController {

	@Autowired
	ClienteService clienteService;

	@Autowired
	ProductoService productoService;

	@RequestMapping("/")
	public String listar() {
		List<Cliente> clientes = clienteService.listar();
		clientes.forEach(System.out::println);
		return "hola";
	}
	
	@RequestMapping("/guardar")
	public String guardar() {
		Cliente cliente = new Cliente();
		cliente.setApellido("APELLIDO 3");
		cliente.setNombre("NOMBRE 3");
		cliente.setCi("3");
		cliente.setCodigo("COD3");
		// cliente.setFechaNacimiento(LocalDate.now());
		cliente.setFechaNacimiento(LocalDate.of(2000, 1, 1));
		// cliente.setFechaAlta(LocalDateTime.now());
		// cliente.setFechaUltimaModificacion(LocalDateTime.now());
		clienteService.guardar(cliente);
		return "guardado";
	}

	@RequestMapping("/actualizar")
	public String actualizar() {
		Cliente cliente = new Cliente();
		cliente.setId(1);
		cliente.setApellido("APELLIDO 2 MODIFICADO");
		cliente.setNombre("NOMBRE 2 MODIFICADO");
		cliente.setCi("2 MODIFICADO");
		cliente.setCodigo("COD2 MODIFICADO");
		// cliente.setFechaNacimiento(LocalDate.now());
		cliente.setFechaNacimiento(LocalDate.of(2000, 1, 1));
		// cliente.setFechaAlta(LocalDateTime.now());
		// cliente.setFechaUltimaModificacion(LocalDateTime.now());
		clienteService.guardar(cliente);
		return "actualizado";
	}

	@RequestMapping("/productos")
	public String listarProducto() {
		List<Producto> productos = productoService.listar();
		productos.forEach(System.out::println);
		return "hola";
	}

	@RequestMapping("/crearProducto")
	public String crearProducto() {
		Producto producto = new Producto();
		producto.setNombre("NOMBRE PRODUCTO 01");
		producto.setCodigo("COD 01");
		producto.setPrecio(BigDecimal.TEN);
		producto.setStock(15);
		productoService.guardar(producto);
		return "guardado";
	}

	@RequestMapping("/actualizarProducto")
	public String actualizarProducto() {
		Producto producto = new Producto();
		producto.setId(31);
		producto.setNombre("NOMBRE PRODUCTO 01 ACTUALIZADO");
		producto.setCodigo("COD 01 ACT.");
		producto.setPrecio(new BigDecimal("15"));
		producto.setStock(16);
		productoService.guardar(producto);
		return "actualizado";
	}

	@RequestMapping("/eliminarProducto")
	public String borrarProducto() {
		productoService.borrar(31);
		return "borrado";
	}
	
}
