package bo.com.bmsc.javaspring.proyecto.services;

import java.time.LocalDateTime;
import java.util.List;

import bo.com.bmsc.javaspring.proyecto.excepciones.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import bo.com.bmsc.javaspring.proyecto.dao.ClienteDAO;
import bo.com.bmsc.javaspring.proyecto.entidades.Cliente;

@Service
public class ClienteService extends BaseService<Cliente> {

	@Autowired
	ClienteDAO clienteDAO;

//	@Override
//	public void borrar(Integer id) {
//		clienteDAO.deleteById(id);
//	}

	@Override
	void validarAlta(Cliente cliente) {
		Cliente clienteExiste = clienteDAO.findByCi(cliente.getCi());
		if(clienteExiste != null && !clienteExiste.getId().equals(cliente.getId())) {
			throw new CustomException("Ya existe un cliente con el CI " + cliente.getCi());
		}
	}

	@Override
	void validarModificacion(Cliente entidadAntigua, Cliente entidadNueva) {
		throw new CustomException("No se permite la actualización de objetos Cliente");
	}

	@Override
	void validarBaja(Cliente entidad) {
		// TODO: Consultar ventas, si hay ventas asociadas a este cliente, no permitir su eliminacion

	}

	@Override
	JpaRepository<Cliente, Integer> getDao() {
		return clienteDAO;
	}

//	Logger logger= LoggerFactory.getLogger(ClienteService.class);
//
//	@Autowired
//	ClienteDAO clienteDAO;
//
//	public Cliente guardar(Cliente cliente){
//		// Log
//		logger.debug("Intenta guardar cliente " + cliente);
//		// Validaciones
//		Cliente clienteExiste = clienteDAO.findByCi(cliente.getCi());
//		if(clienteExiste != null && !clienteExiste.getId().equals(cliente.getId())) {
//			throw new RuntimeException("Ya existe un cliente con el CI " + cliente.getCi());
//		}
//		// Ejecucion de la operacion
//		if (cliente.getId() == null) {
//			cliente.setFechaAlta(LocalDateTime.now());
//		}
//		cliente.setFechaUltimaModificacion(LocalDateTime.now());
//		clienteDAO.saveAndFlush(cliente);
//		// Log de salida
//		logger.debug("Guardó el cliente " + cliente);
//		return cliente;
//	}
//
//	public void borrar(Integer id){
//		logger.debug("Intenta borrar cliente con id " + id);
//		Cliente cliente = clienteDAO.getById(id);
//		if (cliente == null){
//			throw new RuntimeException("No se encontró cliente con ID " + id);
//		}
//		if (cliente.getFechaBaja() != null){
//			throw new RuntimeException("El cliente con ID " + id + " ya se encontraba eliminado");
//		}
//		// otras validaciones...
//
//		cliente.setFechaBaja(LocalDateTime.now());
//		clienteDAO.saveAndFlush(cliente);
//		logger.debug("Eliminó el cliente " + cliente);
//	}
//
//	public Cliente obtener(Integer id){
//		Cliente cliente = clienteDAO.getById(id);
//		if(cliente != null && cliente.getFechaBaja() != null) {
//			return null;
//		}
//		return cliente;
//	}
//
//	//public List<Cliente> listar(){
////		return clienteDAO.findAll().stream()
//	//			.filter(c -> c.getFechaBaja() == null).toList();
//	//}
//
//	public List<Cliente> listar(){
//		return clienteDAO.listarTodos();
//	}
	
}
