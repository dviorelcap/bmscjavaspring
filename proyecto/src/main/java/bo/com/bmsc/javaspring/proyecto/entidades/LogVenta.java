package bo.com.bmsc.javaspring.proyecto.entidades;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CascadeType;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class LogVenta extends EntidadBase{

	@Id
	@GeneratedValue
	private Integer id;
	
	private LocalDateTime fecha;
	
	@Lob
	private String peticion;
	
	@Lob
	private String respuesta;
	
	
}
