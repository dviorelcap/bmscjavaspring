package bo.com.bmsc.spring.boot.sb01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Sb01Application extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(Sb01Application.class);
	}

	@Value("${spring.application.name:Ninguno}")
	private String nombreAplicacion;

	public static void main(String[] args) {
		SpringApplication.run(Sb01Application.class, args);
	}

	Logger logger = LoggerFactory.getLogger(Sb01Application.class);

	@RequestMapping(value = "/")
	public String hello() {
		logger.trace("logTRACE");
		logger.debug("logDEBUG");
		logger.info("logINFO");
		logger.warn("logWARN");
		logger.error("logERROR");
		return "Hola mundo desde Spring Boot!";
	}

	@RequestMapping(value = "/saludo")
	public String helloSaludo() {
		return "Hola desde " + nombreAplicacion;
	}

}
