package bo.com.bmsc.javaspring.consumorest;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ConsumoHttpClientLogin {

    String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNjM4Mjc1ODU2LCJleHAiOjE2MzgyNzk0NTZ9.vX0_1AKhemSrtU1fY8PcGOgF5xhkdDmKT2lXuQqiIzE";

    public static void main(String[] args) throws IOException {
        URL url = new URL("http://localhost:8080/rest/login");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        LoginRequest loginRequest = new LoginRequest("admin", "adminadmin");
        ObjectMapper mapper = new ObjectMapper();
        String input = mapper.writeValueAsString(loginRequest);
        OutputStream os = conn.getOutputStream();
        os.write(input.getBytes());
        os.flush();
        if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
            BufferedReader br2 = new BufferedReader(new InputStreamReader(
                    (conn.getErrorStream())));
            throw new RuntimeException("Fallo " + conn.getResponseCode() + ": " + br2.readLine());
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn.getInputStream())));
        String output;
        System.out.println("Output from Server .... \n");
        while ((output = br.readLine()) != null) {
            System.out.println(output);
        }
        conn.disconnect();
    }

}
