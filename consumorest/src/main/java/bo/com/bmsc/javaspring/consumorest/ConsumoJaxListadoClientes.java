package bo.com.bmsc.javaspring.consumorest;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ConsumoJaxListadoClientes {

    public static void main(String[] args) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/rest/cliente");
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNjM4Mjc3MjE0LCJleHAiOjE2MzgyODA4MTR9.ArGRCbM8ap2s-P3or1JjR3qJ4FcJVuVBDi3D5ZgYjlo";
        Response response = target.request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token)
                .get();
        System.out.println(response.getStatus());
        Cliente[] clientes = response.readEntity(Cliente[].class);
        for (Cliente cliente: clientes){
            System.out.println(cliente);
        }

    }

}
