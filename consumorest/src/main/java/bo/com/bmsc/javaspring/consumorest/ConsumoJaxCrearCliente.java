package bo.com.bmsc.javaspring.consumorest;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.time.Month;

public class ConsumoJaxCrearCliente {

    public static void main(String[] args) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/rest/cliente");
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNjM4Mjc3MjE0LCJleHAiOjE2MzgyODA4MTR9.ArGRCbM8ap2s-P3or1JjR3qJ4FcJVuVBDi3D5ZgYjlo";
        Cliente cliente = new Cliente();
        cliente.setNombre("NombreJax02");
        cliente.setApellido("ApellidoJax02");
        cliente.setCi("ciJax02");
        cliente.setCodigo("codJax02");
        // cliente.setFechaNacimiento("1920-01-01");
        cliente.setFechaNacimiento(LocalDate.of(1920, Month.JANUARY, 1));
        Response response = target.request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token)
                .post(Entity.json(cliente));
        System.out.println(response.getStatus());
        Cliente clienteAgregado = response.readEntity(Cliente.class);
        System.out.println(clienteAgregado);

    }

}
