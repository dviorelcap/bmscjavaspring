package bo.com.bmsc.javaspring.consumorest;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ConsumoHttpClientListadoClientes {

    public static void main(String[] args) throws IOException {
        URL url = new URL("http://localhost:8080/rest/cliente");
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNjM4Mjc1ODU2LCJleHAiOjE2MzgyNzk0NTZ9.vX0_1AKhemSrtU1fY8PcGOgF5xhkdDmKT2lXuQqiIzE";
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Authorization", "Bearer " + token);
        conn.setRequestProperty("Content-Type", "application/json");
        if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Fallo: " + conn.getResponseCode());
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        ObjectMapper mapper = new ObjectMapper();
        String output = br.readLine();
        System.out.println("Output: ");
        System.out.println(output);
        Cliente[] clientes = mapper.readValue(output, Cliente[].class);
        for (Cliente cliente: clientes){
            System.out.println(cliente);
            // cliente.getFechaNacimiento().
        }
        conn.disconnect();

    }

}
