package bo.com.bmsc.javaspring.consumorest;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.Month;

public class ConsumoHttpClientCrearCliente {

    public static void main(String[] args) throws IOException {
        URL url = new URL("http://localhost:8080/rest/cliente");
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNjM4Mjc1ODU2LCJleHAiOjE2MzgyNzk0NTZ9.vX0_1AKhemSrtU1fY8PcGOgF5xhkdDmKT2lXuQqiIzE";
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Authorization", "Bearer " + token);
        conn.setRequestProperty("Content-Type", "application/json");
        ObjectMapper mapper = new ObjectMapper();
        Cliente cliente = new Cliente();
        cliente.setNombre("NombreHttp01");
        cliente.setApellido("ApellidoHttp01");
        cliente.setCi("ciHttp01");
        cliente.setCodigo("codHttp01");
        // cliente.setFechaNacimiento("1920-01-01");
        cliente.setFechaNacimiento(LocalDate.of(1920, Month.JANUARY, 1));
        String input = mapper.writeValueAsString(cliente);
        OutputStream os = conn.getOutputStream();
        os.write(input.getBytes());
        os.flush();
        if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Fallo: " + conn.getResponseCode());
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn.getInputStream())));
        String output = br.readLine();
        Cliente clienteAgregado = mapper.readValue(output, Cliente.class);
        System.out.println(clienteAgregado);
        conn.disconnect();


    }

}
