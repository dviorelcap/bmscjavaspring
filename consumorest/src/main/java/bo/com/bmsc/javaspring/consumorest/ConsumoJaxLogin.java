package bo.com.bmsc.javaspring.consumorest;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ConsumoJaxLogin {
    String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNjM4Mjc3MjE0LCJleHAiOjE2MzgyODA4MTR9.ArGRCbM8ap2s-P3or1JjR3qJ4FcJVuVBDi3D5ZgYjlo";
    public static void main(String[] args) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/rest/login");
        LoginRequest loginRequest = new LoginRequest("admin", "adminadmin");
        Response response = target.request(MediaType.APPLICATION_JSON)
                .post(Entity.json(loginRequest));
        System.out.println(response.getStatus());
        LoginResponse loginResponse = response.readEntity(LoginResponse.class);
        System.out.println(loginResponse);
    }

}
