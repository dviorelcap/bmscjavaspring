package bo.com.bmsc.spring.data.entidades;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Getter
@Setter
@ToString
@Entity
public class Producto {
    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "CODIGO", unique = true)
    private String codigo;
    private String nombre;
    private BigDecimal precio;
}
