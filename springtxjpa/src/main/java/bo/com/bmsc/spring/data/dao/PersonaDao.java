package bo.com.bmsc.spring.data.dao;

import bo.com.bmsc.spring.data.entidades.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonaDao extends JpaRepository<Persona, Integer> {

    List<Persona> findByApellido(String apellido);

    @Query("select p from Persona p where p.nombre = :nombre")
    List<Persona> findByNombre(String nombre);

}
