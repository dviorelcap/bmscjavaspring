package bo.com.bmsc.spring.data.services;

import bo.com.bmsc.spring.data.dao.ProductoDao;
import bo.com.bmsc.spring.data.entidades.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

// @Component
@Service
public class ProductoService {

    @Autowired
    ProductoDao productoDao;

    @Transactional
    public void agregar(Producto producto){
        // Agregar validaciones
        productoDao.save(producto);
    }
}
