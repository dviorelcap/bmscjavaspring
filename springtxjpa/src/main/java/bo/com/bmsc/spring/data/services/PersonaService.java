package bo.com.bmsc.spring.data.services;

import bo.com.bmsc.spring.data.dao.PersonaDao;
import bo.com.bmsc.spring.data.entidades.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

// @Component
@Service
public class PersonaService {

    @Autowired
    PersonaDao personaDao;

    public void agregarPersona(Persona persona){
        // Agregar validaciones
        personaDao.save(persona);
    }

    public void borrar(Integer id){
        personaDao.deleteById(id);
    }

    public List<Persona> buscarPorApellido(String apellido){
        return personaDao.findByApellido(apellido);
    }

}
