package bo.com.bmsc.spring.data.dao;

import bo.com.bmsc.spring.data.entidades.Persona;
import bo.com.bmsc.spring.data.entidades.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductoDao extends JpaRepository<Producto, String> {

}
