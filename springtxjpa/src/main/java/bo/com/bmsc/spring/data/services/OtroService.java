package bo.com.bmsc.spring.data.services;

import bo.com.bmsc.spring.data.dao.PersonaDao;
import bo.com.bmsc.spring.data.dao.ProductoDao;
import bo.com.bmsc.spring.data.entidades.Persona;
import bo.com.bmsc.spring.data.entidades.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class OtroService {

    @Autowired
    PersonaService personaService;

    @Autowired
    ProductoService productoService;

    @Transactional
    public void agregarDosCosas(Persona persona, Producto producto){

        personaService.agregarPersona(persona);
        productoService.agregar(producto);

    }

}
