package bo.com.bmsc.spring.data.entidades;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@ToString
@Entity
public class Persona {
    @Id
    @GeneratedValue
    private Integer id;

    private Integer ci;
    private String nombre;
    private String apellido;
}
