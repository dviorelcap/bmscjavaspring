package bo.com.bmsc.spring.data;


import bo.com.bmsc.spring.data.entidades.Persona;
import bo.com.bmsc.spring.data.entidades.Producto;
import bo.com.bmsc.spring.data.services.OtroService;
import bo.com.bmsc.spring.data.services.PersonaService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.math.BigDecimal;

public class Demo {

    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("ApplicationContext.xml");
        Persona p = new Persona();
        p.setCi(1);
        p.setNombre("Nombre 1");
        p.setApellido("Apellido 1");
        Producto producto = new Producto();
        producto.setCodigo("CODIGO1");
        producto.setNombre("Producto 1");
        producto.setPrecio(BigDecimal.ONE);
        OtroService otroService = (OtroService) context.getBean("otroService");
        otroService.agregarDosCosas(p, producto);

        PersonaService personaService = (PersonaService) context.getBean("personaService");

    }

}
