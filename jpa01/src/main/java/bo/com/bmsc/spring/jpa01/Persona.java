package bo.com.bmsc.spring.jpa01;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@Table(name = "PER_PERSONA")
@NamedQuery(name = "buscarPorNombre", query = "select p from Persona p where p.nombre like :nombre")
public class Persona {

    @Id
    private Integer ci;

    @Column(length = 50, nullable = false)
    private String nombre;

    @Column(name = "aaapellido")
    private String apellido;

}
