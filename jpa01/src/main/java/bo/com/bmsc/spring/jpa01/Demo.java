package bo.com.bmsc.spring.jpa01;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Demo {

    public static void main(String[] args) {
        EntityManagerFactory emf =
                Persistence.createEntityManagerFactory("TestPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Persona p = new Persona();
        p.setCi(12343);
        p.setNombre("Juan");
        p.setApellido("Perez");
        em.persist(p);
        em.getTransaction().commit();
    }
}
