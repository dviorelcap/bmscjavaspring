package bo.com.bmsc.spring.jpa01;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class DemoQuery {

    public static void main(String[] args) {
        EntityManagerFactory emf =
                Persistence.createEntityManagerFactory("TestPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
//        List<Persona> personas =
//                em.createQuery("select p from Persona p").getResultList();
        Query query = em.createQuery("select p from Persona p where p.nombre like :filtro");
        query.setParameter("filtro", "%Nombre%");
        List<Persona> personas = query.getResultList();
        for (Persona persona: personas){
            System.out.println(persona);
            persona.setNombre(persona.getNombre() + " " + persona.getCi());
            // em.remove(persona);
        }
        em.getTransaction().commit();
    }
}
