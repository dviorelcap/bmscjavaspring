/**
 * ApplicationPortService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package bo.com.bmsc.consumo.axis;

public interface ApplicationPortService extends javax.xml.rpc.Service {
    public java.lang.String getApplicationPortSoap11Address();

    public bo.com.bmsc.consumo.axis.ApplicationPort getApplicationPortSoap11() throws javax.xml.rpc.ServiceException;

    public bo.com.bmsc.consumo.axis.ApplicationPort getApplicationPortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
