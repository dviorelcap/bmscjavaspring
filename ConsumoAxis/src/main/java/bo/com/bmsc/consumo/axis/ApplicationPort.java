/**
 * ApplicationPort.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package bo.com.bmsc.consumo.axis;

public interface ApplicationPort extends java.rmi.Remote {
    public bo.com.bmsc.consumo.axis.ClienteDTO[] listarClientes(bo.com.bmsc.consumo.axis.ListarClientesRequest listarClientesRequest) throws java.rmi.RemoteException;
    public bo.com.bmsc.consumo.axis.CrearClientesResponse crearClientes(bo.com.bmsc.consumo.axis.CrearClientesRequest crearClientesRequest) throws java.rmi.RemoteException;
}
