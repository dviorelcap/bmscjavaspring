package bo.com.bmsc.consumo.axis;

import javax.xml.rpc.ServiceException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

public class DemoListar {

    public static void main(String[] args) throws ServiceException, MalformedURLException,
            RemoteException {
        ApplicationPort proxy = new ApplicationPortServiceLocator()
                .getApplicationPortSoap11(new URL("http://localhost:8080/ws"));
        ListarClientesRequest request = new ListarClientesRequest();
        ClienteDTO[] clienteDTOS = proxy.listarClientes(request);
        for (ClienteDTO clienteDTO: clienteDTOS){
            System.out.println("Cliente: " + clienteDTO.getId() + ", " + clienteDTO.getNombre()
            + ", " + clienteDTO.getApellido() + ", " + clienteDTO.getFechaNacimiento());
        }
    }

}
