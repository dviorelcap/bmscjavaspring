/**
 * ApplicationPortServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package bo.com.bmsc.consumo.axis;

public class ApplicationPortServiceLocator extends org.apache.axis.client.Service implements bo.com.bmsc.consumo.axis.ApplicationPortService {

    public ApplicationPortServiceLocator() {
    }


    public ApplicationPortServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ApplicationPortServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ApplicationPortSoap11
    private java.lang.String ApplicationPortSoap11_address = "http://localhost:8080/ws";

    public java.lang.String getApplicationPortSoap11Address() {
        return ApplicationPortSoap11_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ApplicationPortSoap11WSDDServiceName = "ApplicationPortSoap11";

    public java.lang.String getApplicationPortSoap11WSDDServiceName() {
        return ApplicationPortSoap11WSDDServiceName;
    }

    public void setApplicationPortSoap11WSDDServiceName(java.lang.String name) {
        ApplicationPortSoap11WSDDServiceName = name;
    }

    public bo.com.bmsc.consumo.axis.ApplicationPort getApplicationPortSoap11() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ApplicationPortSoap11_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getApplicationPortSoap11(endpoint);
    }

    public bo.com.bmsc.consumo.axis.ApplicationPort getApplicationPortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            bo.com.bmsc.consumo.axis.ApplicationPortSoap11Stub _stub = new bo.com.bmsc.consumo.axis.ApplicationPortSoap11Stub(portAddress, this);
            _stub.setPortName(getApplicationPortSoap11WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setApplicationPortSoap11EndpointAddress(java.lang.String address) {
        ApplicationPortSoap11_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (bo.com.bmsc.consumo.axis.ApplicationPort.class.isAssignableFrom(serviceEndpointInterface)) {
                bo.com.bmsc.consumo.axis.ApplicationPortSoap11Stub _stub = new bo.com.bmsc.consumo.axis.ApplicationPortSoap11Stub(new java.net.URL(ApplicationPortSoap11_address), this);
                _stub.setPortName(getApplicationPortSoap11WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ApplicationPortSoap11".equals(inputPortName)) {
            return getApplicationPortSoap11();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://bmsc.com.bo/javaspring/proyecto/soap/ws", "ApplicationPortService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://bmsc.com.bo/javaspring/proyecto/soap/ws", "ApplicationPortSoap11"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ApplicationPortSoap11".equals(portName)) {
            setApplicationPortSoap11EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
