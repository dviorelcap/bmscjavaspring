public class Ejercicio2v2 {

    static String mostrarVariables(int x, int y, int z){
        return x + ", " + y + ", " + z;
    }

    public static void main(String[] args) {
        int a = 5, b = 6, c = 8;
        if (a < b && a < c){
            System.out.println(mostrarVariables(a, b, c));
        }
        if(a < b && c < a){
            System.out.println(mostrarVariables(c, a, b));
        }
        if(b < a && c < b){
            System.out.println(mostrarVariables(c, b, a));
        }
    }

}
