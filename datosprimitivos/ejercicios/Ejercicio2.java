public class Ejercicio2 {

    static String mostrarVariables(int x, int y, int z){
        return x + ", " + y + ", " + z;
    }

    public static void main(String[] args) {
        int a = 5, b = 10, c = 8;
        if (a < b){
            if (a < c){
                // a es el menor de los tres
                if (b < c){
                    // a, b, c
                    System.out.println(mostrarVariables(a, b, c));
                }
                else{
                    // a, c, b
                    System.out.println(mostrarVariables(a, c, b));
                }
            }
            else{
                // c es el menor de los tres
                if (a < b){
                    // c, a, b
                    System.out.println(mostrarVariables(c, a, b));
                }
                else{
                    // c, b, a
                    System.out.println(mostrarVariables(c, b, a));
                }
            }
        }
        else{
            if (b < c){
                // b es el menor de los tres
                if (a < c){
                    // b, a, c
                    System.out.println(mostrarVariables(b, a, c));
                }
                else{
                    // b, c, a
                    System.out.println(mostrarVariables(b, c, a));
                }
            }
            else{
                // c es el menor de los tres
                if (b < a){
                    // c, b, a
                    System.out.println(mostrarVariables(c, b, a));
                }
                else{
                    // c, a, b
                    System.out.println(mostrarVariables(c, a, b));
                }
            }
        }
    }

}
