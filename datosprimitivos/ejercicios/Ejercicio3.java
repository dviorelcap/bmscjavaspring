public class Ejercicio3 {

    public static void main(String[] args) {
        int a = 3, b = 5;
        String operacion = "*"; // +-*/%
        switch (operacion){
            case "+":
                System.out.println(a + b);
                break;
            case "-":
                System.out.println(a - b);
                break;
            case "*":
                System.out.println(a * b);
                break;
            case "/":
                System.out.println(a / b);
                break;
            case "%":
                System.out.println(a % b);
                break;
            default:
                System.out.println("Operacion no soportada");
                break;
        }
    }

}
