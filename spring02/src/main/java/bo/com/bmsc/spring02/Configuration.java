package bo.com.bmsc.spring02;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@org.springframework.context.annotation.Configuration
@ComponentScan(basePackages = "bo.com.bmsc.spring02")
public class Configuration {

    @Bean
    public Bean1 idBean1(){
        return new Bean1();
    }

    @Bean
    public Bean2 idBean2(){
        return new Bean2();
    }

}
