package bo.com.bmsc.java.ws.contract;

import javax.xml.rpc.ServiceException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;

public interface IWebServiceProxy {

    RespuestaConsultaMovimientosDTO getMovementInfo(ConsultaMovTrxDTO request) throws MalformedURLException, ServiceException, RemoteException;
    RespuestaConsultaSaldoDTO getBalanceInfo(ConsultaSaldoDTO request);

}
