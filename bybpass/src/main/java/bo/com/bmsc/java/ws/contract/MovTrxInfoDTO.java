package bo.com.bmsc.java.ws.contract;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MovTrxInfoDTO {
    private java.lang.String cantidadPuntos;

    private java.lang.String codCatBalance;

    private java.lang.String codCatEECC;

    private java.lang.String codCatFin;

    private java.lang.String codProgramaFidelidad;

    private java.lang.String codigoAutorizacion;

    private java.lang.Short codigoCategoriaComercio;

    private java.lang.String codigoMoneda;

    private java.lang.String cuota;

    private java.lang.String descripcionMovimiento;

    private java.lang.String diasFaltantes;

    private java.lang.Integer fechaOrigen;

    private java.lang.Integer fechaPosteo;

    private java.lang.String flagFacturado;

    private java.lang.Long idMovimientoTransaccion;

    private java.lang.String importe;

    private java.lang.String numeroTarjeta;

    private java.lang.String tipoMovimiento;

    private java.lang.Short tipoRelacionFinanciera;
}
