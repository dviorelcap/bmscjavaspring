package bo.com.bmsc.java.ws;

import bo.com.bmsc.java.ws.contract.*;
import bo.com.bmsc.java.ws.mapper.MapStructMapper;
import org.tempuri.IPaymentMediaMovementsService;
import org.tempuri.PaymentMediaMovementsServiceLocator;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.rpc.ServiceException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

@WebService
public class WebServiceProxy implements IWebServiceProxy {

    @WebMethod
    @WebResult(name = "saludo")
    public String saludar(@WebParam(name = "nombre") String nombre){
        return "hola " + nombre;
    }

    @WebMethod
    @WebResult(name = "respuestaConsultaMovimientosDTO")
    public RespuestaConsultaMovimientosDTO getMovementInfo(@WebParam(name = "consultaMovTrxDTO")ConsultaMovTrxDTO request) throws MalformedURLException, ServiceException, RemoteException {

        System.out.println("Invocacion de getMovementInfo con request " + request);

        org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.ConsultaMovTrxDTO
                requestAtc = MapStructMapper.INSTANCE.mapConsultaMovTrxDTOBmscToAtc(request);
        System.out.println("Conversion de request a " + requestAtc);
//        org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.ConsultaMovTrxDTO requestAtc = new org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.ConsultaMovTrxDTO();
//        requestAtc.setCantidadMovimientos(request.getCantidadMovimientos());
//        requestAtc.setNumeroTarjeta(request.getNumeroTarjeta());
//        requestAtc.setTipoMovimiento(request.getTipoMovimiento());
        System.out.println("Peticion con URL " + Parametros.getUrl());
        IPaymentMediaMovementsService proxy = new PaymentMediaMovementsServiceLocator()
                .getBasicHttpBinding_IPaymentMediaMovementsService(new URL(Parametros.getUrl()));
        org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.RespuestaConsultaMovimientosDTO movementInfoAtc = proxy.getMovementInfo(requestAtc);
        System.out.println("Respuesta de ATC " + movementInfoAtc);
        RespuestaConsultaMovimientosDTO respuesta = MapStructMapper.INSTANCE.mapRespuestaConsultaMovimientosDTOAtcToBmsc(movementInfoAtc);
        System.out.println("Respuesta formato BMSC " + respuesta);
        return respuesta;
    }

    @WebMethod
    @WebResult(name = "respuestaConsultaSaldoDTO")
    public RespuestaConsultaSaldoDTO getBalanceInfo(@WebParam(name = "consultaSaldoDTO")ConsultaSaldoDTO request) {
        return null;
    }
}
