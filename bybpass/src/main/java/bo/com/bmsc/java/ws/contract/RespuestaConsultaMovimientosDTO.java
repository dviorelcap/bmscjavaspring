package bo.com.bmsc.java.ws.contract;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RespuestaConsultaMovimientosDTO {

    private MovTrxInfoDTO[] consultaMovimientosInfo;

    private CabeceraInfoConsultaMovimientosDTO header;

}
