package bo.com.bmsc.java.ws.contract;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CuotasDTO {
    private java.lang.String importeAcumCtas;

    private java.lang.Integer nroCtasPactads;

    private java.lang.Integer nroCuotaActual;
}
