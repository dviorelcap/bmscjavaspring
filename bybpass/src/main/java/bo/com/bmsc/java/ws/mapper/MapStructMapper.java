package bo.com.bmsc.java.ws.mapper;

// import bo.com.bmsc.java.ws.contract.ConsultaMovTrxDTO;
// import bo.com.bmsc.java.ws.contract.RespuestaConsultaMovimientosDTO;
// import bo.com.bmsc.java.ws.contract.MovTrxInfoDTO;
import bo.com.bmsc.java.ws.contract.CabeceraInfoConsultaMovimientosDTO;
import org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.CabeceraInfoDTO;
import org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.ConsultaMovTrxDTO;
import org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.MovTrxInfoDTO;
import org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.RespuestaConsultaMovimientosDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MapStructMapper {

    MapStructMapper INSTANCE = Mappers.getMapper(MapStructMapper.class);

    ConsultaMovTrxDTO mapConsultaMovTrxDTOBmscToAtc(bo.com.bmsc.java.ws.contract.ConsultaMovTrxDTO input);
    bo.com.bmsc.java.ws.contract.ConsultaMovTrxDTO mapConsultaMovTrxDTOAtcToBmsc(ConsultaMovTrxDTO input);

    RespuestaConsultaMovimientosDTO mapRespuestaConsultaMovimientosDTOBmscToAtc(bo.com.bmsc.java.ws.contract.RespuestaConsultaMovimientosDTO input);
    bo.com.bmsc.java.ws.contract.RespuestaConsultaMovimientosDTO mapRespuestaConsultaMovimientosDTOAtcToBmsc(RespuestaConsultaMovimientosDTO input);

    MovTrxInfoDTO mapMovTrxInfoDTOBmscToAtc(bo.com.bmsc.java.ws.contract.MovTrxInfoDTO input);
    bo.com.bmsc.java.ws.contract.MovTrxInfoDTO mapMovTrxInfoDTOAtcToBmsc(MovTrxInfoDTO input);

    CabeceraInfoDTO mapCabeceraInfoConsultaMovimientosDTOBmscToAtc(CabeceraInfoConsultaMovimientosDTO input);
    CabeceraInfoConsultaMovimientosDTO mapCabeceraInfoConsultaMovimientosDTOAtcToBmsc(CabeceraInfoDTO input);

}
