package bo.com.bmsc.java.ws;

public class Parametros {

    private static String url;

    public static void setUrl(String url){
        if (Parametros.url == null) {
            Parametros.url = url;
        }
    }

    public static String getUrl(){
        return url;
    }

}
