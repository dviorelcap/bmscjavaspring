package bo.com.bmsc.java.ws;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class AppContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String url = sce.getServletContext().getInitParameter("urlAtc");
        System.out.println("Leyo de archivo web.xml URL: " + url);
        Parametros.setUrl(url);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
