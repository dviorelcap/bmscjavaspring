package bo.com.bmsc.java.ws.contract;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CabeceraInfoConsultaMovimientosDTO {
    private java.lang.String codigoTerminalCanal;

    private java.lang.String codigoUsuarioAplicativo;

    private java.lang.String numeroOperacion;

}
