package bo.com.bmsc.java.ws.contract;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ConsultaMovTrxDTO {
    private Integer cantidadMovimientos;

    private String codigoBanco;

    private String codigoTerminalCanal;

    private String codigoUsuarioAplicativo;

    private String numeroOperacion;

    private String numeroTarjeta;

    private String tipoMovimiento;
}
