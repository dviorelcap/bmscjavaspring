package bo.com.bmsc.java.ws.contract;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ConsultaSaldoInfoDTO {

    private java.lang.Integer cantAuthPendientes;

    private java.lang.String cicloFacturacion;

    private java.lang.String codAgencia;

    private java.lang.String codEstadoCuenta;

    private java.lang.String codEstadoTarjeta;

    private java.lang.String codGrupoAfinidad;

    private java.lang.Short codMarca;

    private java.lang.String codModeloLiquidacion;

    private java.lang.String codMoneda;

    private java.lang.String codProducto;

    private java.lang.String codSucursal;

    private java.lang.String creditoDisponible;

    private CuotasDTO[] cuotas;

    private java.lang.String descripcionProducto;

    private java.lang.String direccion;

    private java.lang.Integer fechaAltaCuenta;

    private java.lang.Integer fechaEstadoActual;

    private java.lang.Integer fechaProxCierre;

    private java.lang.Integer fechaProxVenPago;

    private java.lang.Integer fechaUltimoCierre;

    private java.lang.Integer fechaUltmoPago;

    private java.lang.Integer fechaVencimientoTarjeta;

    private java.lang.Short formaPago;

    private java.lang.String importeTotalAuthPendientes;

    private java.lang.String limiteCredito;

    private java.lang.String localidad;

    private java.lang.String maxSobregiro;

    private java.lang.String nombreEmpresa;

    private java.lang.String nombreTitular;

    private java.lang.String nroCuenta;

    private java.lang.String nroDocSecundario;

    private java.lang.String nroDocumeto;

    private java.lang.String nroTarjeta;

    private java.lang.String numCuentaBancaria;

    private java.lang.String pagoMinimo;

    private java.lang.String pagoMinimoNoCubierto;

    private java.lang.String saldoActual;

    private java.lang.Short tipoCuenta;

    private java.lang.Short tipoDocumento;


}
