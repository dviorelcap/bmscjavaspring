/**
 * ConsultaMovTrxDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements;

public class ConsultaMovTrxDTO  implements java.io.Serializable {

    @Override
    public String toString() {
        return "ConsultaMovTrxDTO{" +
                "cantidadMovimientos=" + cantidadMovimientos +
                ", codigoBanco='" + codigoBanco + '\'' +
                ", codigoTerminalCanal='" + codigoTerminalCanal + '\'' +
                ", codigoUsuarioAplicativo='" + codigoUsuarioAplicativo + '\'' +
                ", numeroOperacion='" + numeroOperacion + '\'' +
                ", numeroTarjeta='" + numeroTarjeta + '\'' +
                ", tipoMovimiento='" + tipoMovimiento + '\'' +
                '}';
    }

    private java.lang.Integer cantidadMovimientos;

    private java.lang.String codigoBanco;

    private java.lang.String codigoTerminalCanal;

    private java.lang.String codigoUsuarioAplicativo;

    private java.lang.String numeroOperacion;

    private java.lang.String numeroTarjeta;

    private java.lang.String tipoMovimiento;

    public ConsultaMovTrxDTO() {
    }

    public ConsultaMovTrxDTO(
           java.lang.Integer cantidadMovimientos,
           java.lang.String codigoBanco,
           java.lang.String codigoTerminalCanal,
           java.lang.String codigoUsuarioAplicativo,
           java.lang.String numeroOperacion,
           java.lang.String numeroTarjeta,
           java.lang.String tipoMovimiento) {
           this.cantidadMovimientos = cantidadMovimientos;
           this.codigoBanco = codigoBanco;
           this.codigoTerminalCanal = codigoTerminalCanal;
           this.codigoUsuarioAplicativo = codigoUsuarioAplicativo;
           this.numeroOperacion = numeroOperacion;
           this.numeroTarjeta = numeroTarjeta;
           this.tipoMovimiento = tipoMovimiento;
    }


    /**
     * Gets the cantidadMovimientos value for this ConsultaMovTrxDTO.
     * 
     * @return cantidadMovimientos
     */
    public java.lang.Integer getCantidadMovimientos() {
        return cantidadMovimientos;
    }


    /**
     * Sets the cantidadMovimientos value for this ConsultaMovTrxDTO.
     * 
     * @param cantidadMovimientos
     */
    public void setCantidadMovimientos(java.lang.Integer cantidadMovimientos) {
        this.cantidadMovimientos = cantidadMovimientos;
    }


    /**
     * Gets the codigoBanco value for this ConsultaMovTrxDTO.
     * 
     * @return codigoBanco
     */
    public java.lang.String getCodigoBanco() {
        return codigoBanco;
    }


    /**
     * Sets the codigoBanco value for this ConsultaMovTrxDTO.
     * 
     * @param codigoBanco
     */
    public void setCodigoBanco(java.lang.String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }


    /**
     * Gets the codigoTerminalCanal value for this ConsultaMovTrxDTO.
     * 
     * @return codigoTerminalCanal
     */
    public java.lang.String getCodigoTerminalCanal() {
        return codigoTerminalCanal;
    }


    /**
     * Sets the codigoTerminalCanal value for this ConsultaMovTrxDTO.
     * 
     * @param codigoTerminalCanal
     */
    public void setCodigoTerminalCanal(java.lang.String codigoTerminalCanal) {
        this.codigoTerminalCanal = codigoTerminalCanal;
    }


    /**
     * Gets the codigoUsuarioAplicativo value for this ConsultaMovTrxDTO.
     * 
     * @return codigoUsuarioAplicativo
     */
    public java.lang.String getCodigoUsuarioAplicativo() {
        return codigoUsuarioAplicativo;
    }


    /**
     * Sets the codigoUsuarioAplicativo value for this ConsultaMovTrxDTO.
     * 
     * @param codigoUsuarioAplicativo
     */
    public void setCodigoUsuarioAplicativo(java.lang.String codigoUsuarioAplicativo) {
        this.codigoUsuarioAplicativo = codigoUsuarioAplicativo;
    }


    /**
     * Gets the numeroOperacion value for this ConsultaMovTrxDTO.
     * 
     * @return numeroOperacion
     */
    public java.lang.String getNumeroOperacion() {
        return numeroOperacion;
    }


    /**
     * Sets the numeroOperacion value for this ConsultaMovTrxDTO.
     * 
     * @param numeroOperacion
     */
    public void setNumeroOperacion(java.lang.String numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }


    /**
     * Gets the numeroTarjeta value for this ConsultaMovTrxDTO.
     * 
     * @return numeroTarjeta
     */
    public java.lang.String getNumeroTarjeta() {
        return numeroTarjeta;
    }


    /**
     * Sets the numeroTarjeta value for this ConsultaMovTrxDTO.
     * 
     * @param numeroTarjeta
     */
    public void setNumeroTarjeta(java.lang.String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }


    /**
     * Gets the tipoMovimiento value for this ConsultaMovTrxDTO.
     * 
     * @return tipoMovimiento
     */
    public java.lang.String getTipoMovimiento() {
        return tipoMovimiento;
    }


    /**
     * Sets the tipoMovimiento value for this ConsultaMovTrxDTO.
     * 
     * @param tipoMovimiento
     */
    public void setTipoMovimiento(java.lang.String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConsultaMovTrxDTO)) return false;
        ConsultaMovTrxDTO other = (ConsultaMovTrxDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cantidadMovimientos==null && other.getCantidadMovimientos()==null) || 
             (this.cantidadMovimientos!=null &&
              this.cantidadMovimientos.equals(other.getCantidadMovimientos()))) &&
            ((this.codigoBanco==null && other.getCodigoBanco()==null) || 
             (this.codigoBanco!=null &&
              this.codigoBanco.equals(other.getCodigoBanco()))) &&
            ((this.codigoTerminalCanal==null && other.getCodigoTerminalCanal()==null) || 
             (this.codigoTerminalCanal!=null &&
              this.codigoTerminalCanal.equals(other.getCodigoTerminalCanal()))) &&
            ((this.codigoUsuarioAplicativo==null && other.getCodigoUsuarioAplicativo()==null) || 
             (this.codigoUsuarioAplicativo!=null &&
              this.codigoUsuarioAplicativo.equals(other.getCodigoUsuarioAplicativo()))) &&
            ((this.numeroOperacion==null && other.getNumeroOperacion()==null) || 
             (this.numeroOperacion!=null &&
              this.numeroOperacion.equals(other.getNumeroOperacion()))) &&
            ((this.numeroTarjeta==null && other.getNumeroTarjeta()==null) || 
             (this.numeroTarjeta!=null &&
              this.numeroTarjeta.equals(other.getNumeroTarjeta()))) &&
            ((this.tipoMovimiento==null && other.getTipoMovimiento()==null) || 
             (this.tipoMovimiento!=null &&
              this.tipoMovimiento.equals(other.getTipoMovimiento())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCantidadMovimientos() != null) {
            _hashCode += getCantidadMovimientos().hashCode();
        }
        if (getCodigoBanco() != null) {
            _hashCode += getCodigoBanco().hashCode();
        }
        if (getCodigoTerminalCanal() != null) {
            _hashCode += getCodigoTerminalCanal().hashCode();
        }
        if (getCodigoUsuarioAplicativo() != null) {
            _hashCode += getCodigoUsuarioAplicativo().hashCode();
        }
        if (getNumeroOperacion() != null) {
            _hashCode += getNumeroOperacion().hashCode();
        }
        if (getNumeroTarjeta() != null) {
            _hashCode += getNumeroTarjeta().hashCode();
        }
        if (getTipoMovimiento() != null) {
            _hashCode += getTipoMovimiento().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConsultaMovTrxDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "ConsultaMovTrxDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cantidadMovimientos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "CantidadMovimientos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoBanco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "CodigoBanco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoTerminalCanal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "CodigoTerminalCanal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoUsuarioAplicativo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "CodigoUsuarioAplicativo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroOperacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "NumeroOperacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroTarjeta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "NumeroTarjeta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoMovimiento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "TipoMovimiento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
