/**
 * CabeceraInfoDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance;

public class CabeceraInfoDTO  implements java.io.Serializable {
    private java.lang.String codigoBanco;

    private java.lang.String codigoMatriculaSolicitante;

    private java.lang.String codigoTerminalCanal;

    private java.lang.String codigoUsuarioAplicativo;

    private java.lang.String numeroOperacion;

    public CabeceraInfoDTO() {
    }

    public CabeceraInfoDTO(
           java.lang.String codigoBanco,
           java.lang.String codigoMatriculaSolicitante,
           java.lang.String codigoTerminalCanal,
           java.lang.String codigoUsuarioAplicativo,
           java.lang.String numeroOperacion) {
           this.codigoBanco = codigoBanco;
           this.codigoMatriculaSolicitante = codigoMatriculaSolicitante;
           this.codigoTerminalCanal = codigoTerminalCanal;
           this.codigoUsuarioAplicativo = codigoUsuarioAplicativo;
           this.numeroOperacion = numeroOperacion;
    }


    /**
     * Gets the codigoBanco value for this CabeceraInfoDTO.
     * 
     * @return codigoBanco
     */
    public java.lang.String getCodigoBanco() {
        return codigoBanco;
    }


    /**
     * Sets the codigoBanco value for this CabeceraInfoDTO.
     * 
     * @param codigoBanco
     */
    public void setCodigoBanco(java.lang.String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }


    /**
     * Gets the codigoMatriculaSolicitante value for this CabeceraInfoDTO.
     * 
     * @return codigoMatriculaSolicitante
     */
    public java.lang.String getCodigoMatriculaSolicitante() {
        return codigoMatriculaSolicitante;
    }


    /**
     * Sets the codigoMatriculaSolicitante value for this CabeceraInfoDTO.
     * 
     * @param codigoMatriculaSolicitante
     */
    public void setCodigoMatriculaSolicitante(java.lang.String codigoMatriculaSolicitante) {
        this.codigoMatriculaSolicitante = codigoMatriculaSolicitante;
    }


    /**
     * Gets the codigoTerminalCanal value for this CabeceraInfoDTO.
     * 
     * @return codigoTerminalCanal
     */
    public java.lang.String getCodigoTerminalCanal() {
        return codigoTerminalCanal;
    }


    /**
     * Sets the codigoTerminalCanal value for this CabeceraInfoDTO.
     * 
     * @param codigoTerminalCanal
     */
    public void setCodigoTerminalCanal(java.lang.String codigoTerminalCanal) {
        this.codigoTerminalCanal = codigoTerminalCanal;
    }


    /**
     * Gets the codigoUsuarioAplicativo value for this CabeceraInfoDTO.
     * 
     * @return codigoUsuarioAplicativo
     */
    public java.lang.String getCodigoUsuarioAplicativo() {
        return codigoUsuarioAplicativo;
    }


    /**
     * Sets the codigoUsuarioAplicativo value for this CabeceraInfoDTO.
     * 
     * @param codigoUsuarioAplicativo
     */
    public void setCodigoUsuarioAplicativo(java.lang.String codigoUsuarioAplicativo) {
        this.codigoUsuarioAplicativo = codigoUsuarioAplicativo;
    }


    /**
     * Gets the numeroOperacion value for this CabeceraInfoDTO.
     * 
     * @return numeroOperacion
     */
    public java.lang.String getNumeroOperacion() {
        return numeroOperacion;
    }


    /**
     * Sets the numeroOperacion value for this CabeceraInfoDTO.
     * 
     * @param numeroOperacion
     */
    public void setNumeroOperacion(java.lang.String numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CabeceraInfoDTO)) return false;
        CabeceraInfoDTO other = (CabeceraInfoDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codigoBanco==null && other.getCodigoBanco()==null) || 
             (this.codigoBanco!=null &&
              this.codigoBanco.equals(other.getCodigoBanco()))) &&
            ((this.codigoMatriculaSolicitante==null && other.getCodigoMatriculaSolicitante()==null) || 
             (this.codigoMatriculaSolicitante!=null &&
              this.codigoMatriculaSolicitante.equals(other.getCodigoMatriculaSolicitante()))) &&
            ((this.codigoTerminalCanal==null && other.getCodigoTerminalCanal()==null) || 
             (this.codigoTerminalCanal!=null &&
              this.codigoTerminalCanal.equals(other.getCodigoTerminalCanal()))) &&
            ((this.codigoUsuarioAplicativo==null && other.getCodigoUsuarioAplicativo()==null) || 
             (this.codigoUsuarioAplicativo!=null &&
              this.codigoUsuarioAplicativo.equals(other.getCodigoUsuarioAplicativo()))) &&
            ((this.numeroOperacion==null && other.getNumeroOperacion()==null) || 
             (this.numeroOperacion!=null &&
              this.numeroOperacion.equals(other.getNumeroOperacion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodigoBanco() != null) {
            _hashCode += getCodigoBanco().hashCode();
        }
        if (getCodigoMatriculaSolicitante() != null) {
            _hashCode += getCodigoMatriculaSolicitante().hashCode();
        }
        if (getCodigoTerminalCanal() != null) {
            _hashCode += getCodigoTerminalCanal().hashCode();
        }
        if (getCodigoUsuarioAplicativo() != null) {
            _hashCode += getCodigoUsuarioAplicativo().hashCode();
        }
        if (getNumeroOperacion() != null) {
            _hashCode += getNumeroOperacion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CabeceraInfoDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CabeceraInfoDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoBanco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodigoBanco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoMatriculaSolicitante");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodigoMatriculaSolicitante"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoTerminalCanal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodigoTerminalCanal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoUsuarioAplicativo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodigoUsuarioAplicativo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroOperacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "NumeroOperacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
