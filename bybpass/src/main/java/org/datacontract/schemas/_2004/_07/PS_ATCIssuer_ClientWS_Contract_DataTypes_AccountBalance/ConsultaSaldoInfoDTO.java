/**
 * ConsultaSaldoInfoDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance;

public class ConsultaSaldoInfoDTO  implements java.io.Serializable {
    private java.lang.Integer cantAuthPendientes;

    private java.lang.String cicloFacturacion;

    private java.lang.String codAgencia;

    private java.lang.String codEstadoCuenta;

    private java.lang.String codEstadoTarjeta;

    private java.lang.String codGrupoAfinidad;

    private java.lang.Short codMarca;

    private java.lang.String codModeloLiquidacion;

    private java.lang.String codMoneda;

    private java.lang.String codProducto;

    private java.lang.String codSucursal;

    private java.lang.String creditoDisponible;

    private org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance.CuotasDTO[] cuotas;

    private java.lang.String descripcionProducto;

    private java.lang.String direccion;

    private java.lang.Integer fechaAltaCuenta;

    private java.lang.Integer fechaEstadoActual;

    private java.lang.Integer fechaProxCierre;

    private java.lang.Integer fechaProxVenPago;

    private java.lang.Integer fechaUltimoCierre;

    private java.lang.Integer fechaUltmoPago;

    private java.lang.Integer fechaVencimientoTarjeta;

    private java.lang.Short formaPago;

    private java.lang.String importeTotalAuthPendientes;

    private java.lang.String limiteCredito;

    private java.lang.String localidad;

    private java.lang.String maxSobregiro;

    private java.lang.String nombreEmpresa;

    private java.lang.String nombreTitular;

    private java.lang.String nroCuenta;

    private java.lang.String nroDocSecundario;

    private java.lang.String nroDocumeto;

    private java.lang.String nroTarjeta;

    private java.lang.String numCuentaBancaria;

    private java.lang.String pagoMinimo;

    private java.lang.String pagoMinimoNoCubierto;

    private java.lang.String saldoActual;

    private java.lang.Short tipoCuenta;

    private java.lang.Short tipoDocumento;

    public ConsultaSaldoInfoDTO() {
    }

    public ConsultaSaldoInfoDTO(
           java.lang.Integer cantAuthPendientes,
           java.lang.String cicloFacturacion,
           java.lang.String codAgencia,
           java.lang.String codEstadoCuenta,
           java.lang.String codEstadoTarjeta,
           java.lang.String codGrupoAfinidad,
           java.lang.Short codMarca,
           java.lang.String codModeloLiquidacion,
           java.lang.String codMoneda,
           java.lang.String codProducto,
           java.lang.String codSucursal,
           java.lang.String creditoDisponible,
           org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance.CuotasDTO[] cuotas,
           java.lang.String descripcionProducto,
           java.lang.String direccion,
           java.lang.Integer fechaAltaCuenta,
           java.lang.Integer fechaEstadoActual,
           java.lang.Integer fechaProxCierre,
           java.lang.Integer fechaProxVenPago,
           java.lang.Integer fechaUltimoCierre,
           java.lang.Integer fechaUltmoPago,
           java.lang.Integer fechaVencimientoTarjeta,
           java.lang.Short formaPago,
           java.lang.String importeTotalAuthPendientes,
           java.lang.String limiteCredito,
           java.lang.String localidad,
           java.lang.String maxSobregiro,
           java.lang.String nombreEmpresa,
           java.lang.String nombreTitular,
           java.lang.String nroCuenta,
           java.lang.String nroDocSecundario,
           java.lang.String nroDocumeto,
           java.lang.String nroTarjeta,
           java.lang.String numCuentaBancaria,
           java.lang.String pagoMinimo,
           java.lang.String pagoMinimoNoCubierto,
           java.lang.String saldoActual,
           java.lang.Short tipoCuenta,
           java.lang.Short tipoDocumento) {
           this.cantAuthPendientes = cantAuthPendientes;
           this.cicloFacturacion = cicloFacturacion;
           this.codAgencia = codAgencia;
           this.codEstadoCuenta = codEstadoCuenta;
           this.codEstadoTarjeta = codEstadoTarjeta;
           this.codGrupoAfinidad = codGrupoAfinidad;
           this.codMarca = codMarca;
           this.codModeloLiquidacion = codModeloLiquidacion;
           this.codMoneda = codMoneda;
           this.codProducto = codProducto;
           this.codSucursal = codSucursal;
           this.creditoDisponible = creditoDisponible;
           this.cuotas = cuotas;
           this.descripcionProducto = descripcionProducto;
           this.direccion = direccion;
           this.fechaAltaCuenta = fechaAltaCuenta;
           this.fechaEstadoActual = fechaEstadoActual;
           this.fechaProxCierre = fechaProxCierre;
           this.fechaProxVenPago = fechaProxVenPago;
           this.fechaUltimoCierre = fechaUltimoCierre;
           this.fechaUltmoPago = fechaUltmoPago;
           this.fechaVencimientoTarjeta = fechaVencimientoTarjeta;
           this.formaPago = formaPago;
           this.importeTotalAuthPendientes = importeTotalAuthPendientes;
           this.limiteCredito = limiteCredito;
           this.localidad = localidad;
           this.maxSobregiro = maxSobregiro;
           this.nombreEmpresa = nombreEmpresa;
           this.nombreTitular = nombreTitular;
           this.nroCuenta = nroCuenta;
           this.nroDocSecundario = nroDocSecundario;
           this.nroDocumeto = nroDocumeto;
           this.nroTarjeta = nroTarjeta;
           this.numCuentaBancaria = numCuentaBancaria;
           this.pagoMinimo = pagoMinimo;
           this.pagoMinimoNoCubierto = pagoMinimoNoCubierto;
           this.saldoActual = saldoActual;
           this.tipoCuenta = tipoCuenta;
           this.tipoDocumento = tipoDocumento;
    }


    /**
     * Gets the cantAuthPendientes value for this ConsultaSaldoInfoDTO.
     * 
     * @return cantAuthPendientes
     */
    public java.lang.Integer getCantAuthPendientes() {
        return cantAuthPendientes;
    }


    /**
     * Sets the cantAuthPendientes value for this ConsultaSaldoInfoDTO.
     * 
     * @param cantAuthPendientes
     */
    public void setCantAuthPendientes(java.lang.Integer cantAuthPendientes) {
        this.cantAuthPendientes = cantAuthPendientes;
    }


    /**
     * Gets the cicloFacturacion value for this ConsultaSaldoInfoDTO.
     * 
     * @return cicloFacturacion
     */
    public java.lang.String getCicloFacturacion() {
        return cicloFacturacion;
    }


    /**
     * Sets the cicloFacturacion value for this ConsultaSaldoInfoDTO.
     * 
     * @param cicloFacturacion
     */
    public void setCicloFacturacion(java.lang.String cicloFacturacion) {
        this.cicloFacturacion = cicloFacturacion;
    }


    /**
     * Gets the codAgencia value for this ConsultaSaldoInfoDTO.
     * 
     * @return codAgencia
     */
    public java.lang.String getCodAgencia() {
        return codAgencia;
    }


    /**
     * Sets the codAgencia value for this ConsultaSaldoInfoDTO.
     * 
     * @param codAgencia
     */
    public void setCodAgencia(java.lang.String codAgencia) {
        this.codAgencia = codAgencia;
    }


    /**
     * Gets the codEstadoCuenta value for this ConsultaSaldoInfoDTO.
     * 
     * @return codEstadoCuenta
     */
    public java.lang.String getCodEstadoCuenta() {
        return codEstadoCuenta;
    }


    /**
     * Sets the codEstadoCuenta value for this ConsultaSaldoInfoDTO.
     * 
     * @param codEstadoCuenta
     */
    public void setCodEstadoCuenta(java.lang.String codEstadoCuenta) {
        this.codEstadoCuenta = codEstadoCuenta;
    }


    /**
     * Gets the codEstadoTarjeta value for this ConsultaSaldoInfoDTO.
     * 
     * @return codEstadoTarjeta
     */
    public java.lang.String getCodEstadoTarjeta() {
        return codEstadoTarjeta;
    }


    /**
     * Sets the codEstadoTarjeta value for this ConsultaSaldoInfoDTO.
     * 
     * @param codEstadoTarjeta
     */
    public void setCodEstadoTarjeta(java.lang.String codEstadoTarjeta) {
        this.codEstadoTarjeta = codEstadoTarjeta;
    }


    /**
     * Gets the codGrupoAfinidad value for this ConsultaSaldoInfoDTO.
     * 
     * @return codGrupoAfinidad
     */
    public java.lang.String getCodGrupoAfinidad() {
        return codGrupoAfinidad;
    }


    /**
     * Sets the codGrupoAfinidad value for this ConsultaSaldoInfoDTO.
     * 
     * @param codGrupoAfinidad
     */
    public void setCodGrupoAfinidad(java.lang.String codGrupoAfinidad) {
        this.codGrupoAfinidad = codGrupoAfinidad;
    }


    /**
     * Gets the codMarca value for this ConsultaSaldoInfoDTO.
     * 
     * @return codMarca
     */
    public java.lang.Short getCodMarca() {
        return codMarca;
    }


    /**
     * Sets the codMarca value for this ConsultaSaldoInfoDTO.
     * 
     * @param codMarca
     */
    public void setCodMarca(java.lang.Short codMarca) {
        this.codMarca = codMarca;
    }


    /**
     * Gets the codModeloLiquidacion value for this ConsultaSaldoInfoDTO.
     * 
     * @return codModeloLiquidacion
     */
    public java.lang.String getCodModeloLiquidacion() {
        return codModeloLiquidacion;
    }


    /**
     * Sets the codModeloLiquidacion value for this ConsultaSaldoInfoDTO.
     * 
     * @param codModeloLiquidacion
     */
    public void setCodModeloLiquidacion(java.lang.String codModeloLiquidacion) {
        this.codModeloLiquidacion = codModeloLiquidacion;
    }


    /**
     * Gets the codMoneda value for this ConsultaSaldoInfoDTO.
     * 
     * @return codMoneda
     */
    public java.lang.String getCodMoneda() {
        return codMoneda;
    }


    /**
     * Sets the codMoneda value for this ConsultaSaldoInfoDTO.
     * 
     * @param codMoneda
     */
    public void setCodMoneda(java.lang.String codMoneda) {
        this.codMoneda = codMoneda;
    }


    /**
     * Gets the codProducto value for this ConsultaSaldoInfoDTO.
     * 
     * @return codProducto
     */
    public java.lang.String getCodProducto() {
        return codProducto;
    }


    /**
     * Sets the codProducto value for this ConsultaSaldoInfoDTO.
     * 
     * @param codProducto
     */
    public void setCodProducto(java.lang.String codProducto) {
        this.codProducto = codProducto;
    }


    /**
     * Gets the codSucursal value for this ConsultaSaldoInfoDTO.
     * 
     * @return codSucursal
     */
    public java.lang.String getCodSucursal() {
        return codSucursal;
    }


    /**
     * Sets the codSucursal value for this ConsultaSaldoInfoDTO.
     * 
     * @param codSucursal
     */
    public void setCodSucursal(java.lang.String codSucursal) {
        this.codSucursal = codSucursal;
    }


    /**
     * Gets the creditoDisponible value for this ConsultaSaldoInfoDTO.
     * 
     * @return creditoDisponible
     */
    public java.lang.String getCreditoDisponible() {
        return creditoDisponible;
    }


    /**
     * Sets the creditoDisponible value for this ConsultaSaldoInfoDTO.
     * 
     * @param creditoDisponible
     */
    public void setCreditoDisponible(java.lang.String creditoDisponible) {
        this.creditoDisponible = creditoDisponible;
    }


    /**
     * Gets the cuotas value for this ConsultaSaldoInfoDTO.
     * 
     * @return cuotas
     */
    public org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance.CuotasDTO[] getCuotas() {
        return cuotas;
    }


    /**
     * Sets the cuotas value for this ConsultaSaldoInfoDTO.
     * 
     * @param cuotas
     */
    public void setCuotas(org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance.CuotasDTO[] cuotas) {
        this.cuotas = cuotas;
    }


    /**
     * Gets the descripcionProducto value for this ConsultaSaldoInfoDTO.
     * 
     * @return descripcionProducto
     */
    public java.lang.String getDescripcionProducto() {
        return descripcionProducto;
    }


    /**
     * Sets the descripcionProducto value for this ConsultaSaldoInfoDTO.
     * 
     * @param descripcionProducto
     */
    public void setDescripcionProducto(java.lang.String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }


    /**
     * Gets the direccion value for this ConsultaSaldoInfoDTO.
     * 
     * @return direccion
     */
    public java.lang.String getDireccion() {
        return direccion;
    }


    /**
     * Sets the direccion value for this ConsultaSaldoInfoDTO.
     * 
     * @param direccion
     */
    public void setDireccion(java.lang.String direccion) {
        this.direccion = direccion;
    }


    /**
     * Gets the fechaAltaCuenta value for this ConsultaSaldoInfoDTO.
     * 
     * @return fechaAltaCuenta
     */
    public java.lang.Integer getFechaAltaCuenta() {
        return fechaAltaCuenta;
    }


    /**
     * Sets the fechaAltaCuenta value for this ConsultaSaldoInfoDTO.
     * 
     * @param fechaAltaCuenta
     */
    public void setFechaAltaCuenta(java.lang.Integer fechaAltaCuenta) {
        this.fechaAltaCuenta = fechaAltaCuenta;
    }


    /**
     * Gets the fechaEstadoActual value for this ConsultaSaldoInfoDTO.
     * 
     * @return fechaEstadoActual
     */
    public java.lang.Integer getFechaEstadoActual() {
        return fechaEstadoActual;
    }


    /**
     * Sets the fechaEstadoActual value for this ConsultaSaldoInfoDTO.
     * 
     * @param fechaEstadoActual
     */
    public void setFechaEstadoActual(java.lang.Integer fechaEstadoActual) {
        this.fechaEstadoActual = fechaEstadoActual;
    }


    /**
     * Gets the fechaProxCierre value for this ConsultaSaldoInfoDTO.
     * 
     * @return fechaProxCierre
     */
    public java.lang.Integer getFechaProxCierre() {
        return fechaProxCierre;
    }


    /**
     * Sets the fechaProxCierre value for this ConsultaSaldoInfoDTO.
     * 
     * @param fechaProxCierre
     */
    public void setFechaProxCierre(java.lang.Integer fechaProxCierre) {
        this.fechaProxCierre = fechaProxCierre;
    }


    /**
     * Gets the fechaProxVenPago value for this ConsultaSaldoInfoDTO.
     * 
     * @return fechaProxVenPago
     */
    public java.lang.Integer getFechaProxVenPago() {
        return fechaProxVenPago;
    }


    /**
     * Sets the fechaProxVenPago value for this ConsultaSaldoInfoDTO.
     * 
     * @param fechaProxVenPago
     */
    public void setFechaProxVenPago(java.lang.Integer fechaProxVenPago) {
        this.fechaProxVenPago = fechaProxVenPago;
    }


    /**
     * Gets the fechaUltimoCierre value for this ConsultaSaldoInfoDTO.
     * 
     * @return fechaUltimoCierre
     */
    public java.lang.Integer getFechaUltimoCierre() {
        return fechaUltimoCierre;
    }


    /**
     * Sets the fechaUltimoCierre value for this ConsultaSaldoInfoDTO.
     * 
     * @param fechaUltimoCierre
     */
    public void setFechaUltimoCierre(java.lang.Integer fechaUltimoCierre) {
        this.fechaUltimoCierre = fechaUltimoCierre;
    }


    /**
     * Gets the fechaUltmoPago value for this ConsultaSaldoInfoDTO.
     * 
     * @return fechaUltmoPago
     */
    public java.lang.Integer getFechaUltmoPago() {
        return fechaUltmoPago;
    }


    /**
     * Sets the fechaUltmoPago value for this ConsultaSaldoInfoDTO.
     * 
     * @param fechaUltmoPago
     */
    public void setFechaUltmoPago(java.lang.Integer fechaUltmoPago) {
        this.fechaUltmoPago = fechaUltmoPago;
    }


    /**
     * Gets the fechaVencimientoTarjeta value for this ConsultaSaldoInfoDTO.
     * 
     * @return fechaVencimientoTarjeta
     */
    public java.lang.Integer getFechaVencimientoTarjeta() {
        return fechaVencimientoTarjeta;
    }


    /**
     * Sets the fechaVencimientoTarjeta value for this ConsultaSaldoInfoDTO.
     * 
     * @param fechaVencimientoTarjeta
     */
    public void setFechaVencimientoTarjeta(java.lang.Integer fechaVencimientoTarjeta) {
        this.fechaVencimientoTarjeta = fechaVencimientoTarjeta;
    }


    /**
     * Gets the formaPago value for this ConsultaSaldoInfoDTO.
     * 
     * @return formaPago
     */
    public java.lang.Short getFormaPago() {
        return formaPago;
    }


    /**
     * Sets the formaPago value for this ConsultaSaldoInfoDTO.
     * 
     * @param formaPago
     */
    public void setFormaPago(java.lang.Short formaPago) {
        this.formaPago = formaPago;
    }


    /**
     * Gets the importeTotalAuthPendientes value for this ConsultaSaldoInfoDTO.
     * 
     * @return importeTotalAuthPendientes
     */
    public java.lang.String getImporteTotalAuthPendientes() {
        return importeTotalAuthPendientes;
    }


    /**
     * Sets the importeTotalAuthPendientes value for this ConsultaSaldoInfoDTO.
     * 
     * @param importeTotalAuthPendientes
     */
    public void setImporteTotalAuthPendientes(java.lang.String importeTotalAuthPendientes) {
        this.importeTotalAuthPendientes = importeTotalAuthPendientes;
    }


    /**
     * Gets the limiteCredito value for this ConsultaSaldoInfoDTO.
     * 
     * @return limiteCredito
     */
    public java.lang.String getLimiteCredito() {
        return limiteCredito;
    }


    /**
     * Sets the limiteCredito value for this ConsultaSaldoInfoDTO.
     * 
     * @param limiteCredito
     */
    public void setLimiteCredito(java.lang.String limiteCredito) {
        this.limiteCredito = limiteCredito;
    }


    /**
     * Gets the localidad value for this ConsultaSaldoInfoDTO.
     * 
     * @return localidad
     */
    public java.lang.String getLocalidad() {
        return localidad;
    }


    /**
     * Sets the localidad value for this ConsultaSaldoInfoDTO.
     * 
     * @param localidad
     */
    public void setLocalidad(java.lang.String localidad) {
        this.localidad = localidad;
    }


    /**
     * Gets the maxSobregiro value for this ConsultaSaldoInfoDTO.
     * 
     * @return maxSobregiro
     */
    public java.lang.String getMaxSobregiro() {
        return maxSobregiro;
    }


    /**
     * Sets the maxSobregiro value for this ConsultaSaldoInfoDTO.
     * 
     * @param maxSobregiro
     */
    public void setMaxSobregiro(java.lang.String maxSobregiro) {
        this.maxSobregiro = maxSobregiro;
    }


    /**
     * Gets the nombreEmpresa value for this ConsultaSaldoInfoDTO.
     * 
     * @return nombreEmpresa
     */
    public java.lang.String getNombreEmpresa() {
        return nombreEmpresa;
    }


    /**
     * Sets the nombreEmpresa value for this ConsultaSaldoInfoDTO.
     * 
     * @param nombreEmpresa
     */
    public void setNombreEmpresa(java.lang.String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }


    /**
     * Gets the nombreTitular value for this ConsultaSaldoInfoDTO.
     * 
     * @return nombreTitular
     */
    public java.lang.String getNombreTitular() {
        return nombreTitular;
    }


    /**
     * Sets the nombreTitular value for this ConsultaSaldoInfoDTO.
     * 
     * @param nombreTitular
     */
    public void setNombreTitular(java.lang.String nombreTitular) {
        this.nombreTitular = nombreTitular;
    }


    /**
     * Gets the nroCuenta value for this ConsultaSaldoInfoDTO.
     * 
     * @return nroCuenta
     */
    public java.lang.String getNroCuenta() {
        return nroCuenta;
    }


    /**
     * Sets the nroCuenta value for this ConsultaSaldoInfoDTO.
     * 
     * @param nroCuenta
     */
    public void setNroCuenta(java.lang.String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }


    /**
     * Gets the nroDocSecundario value for this ConsultaSaldoInfoDTO.
     * 
     * @return nroDocSecundario
     */
    public java.lang.String getNroDocSecundario() {
        return nroDocSecundario;
    }


    /**
     * Sets the nroDocSecundario value for this ConsultaSaldoInfoDTO.
     * 
     * @param nroDocSecundario
     */
    public void setNroDocSecundario(java.lang.String nroDocSecundario) {
        this.nroDocSecundario = nroDocSecundario;
    }


    /**
     * Gets the nroDocumeto value for this ConsultaSaldoInfoDTO.
     * 
     * @return nroDocumeto
     */
    public java.lang.String getNroDocumeto() {
        return nroDocumeto;
    }


    /**
     * Sets the nroDocumeto value for this ConsultaSaldoInfoDTO.
     * 
     * @param nroDocumeto
     */
    public void setNroDocumeto(java.lang.String nroDocumeto) {
        this.nroDocumeto = nroDocumeto;
    }


    /**
     * Gets the nroTarjeta value for this ConsultaSaldoInfoDTO.
     * 
     * @return nroTarjeta
     */
    public java.lang.String getNroTarjeta() {
        return nroTarjeta;
    }


    /**
     * Sets the nroTarjeta value for this ConsultaSaldoInfoDTO.
     * 
     * @param nroTarjeta
     */
    public void setNroTarjeta(java.lang.String nroTarjeta) {
        this.nroTarjeta = nroTarjeta;
    }


    /**
     * Gets the numCuentaBancaria value for this ConsultaSaldoInfoDTO.
     * 
     * @return numCuentaBancaria
     */
    public java.lang.String getNumCuentaBancaria() {
        return numCuentaBancaria;
    }


    /**
     * Sets the numCuentaBancaria value for this ConsultaSaldoInfoDTO.
     * 
     * @param numCuentaBancaria
     */
    public void setNumCuentaBancaria(java.lang.String numCuentaBancaria) {
        this.numCuentaBancaria = numCuentaBancaria;
    }


    /**
     * Gets the pagoMinimo value for this ConsultaSaldoInfoDTO.
     * 
     * @return pagoMinimo
     */
    public java.lang.String getPagoMinimo() {
        return pagoMinimo;
    }


    /**
     * Sets the pagoMinimo value for this ConsultaSaldoInfoDTO.
     * 
     * @param pagoMinimo
     */
    public void setPagoMinimo(java.lang.String pagoMinimo) {
        this.pagoMinimo = pagoMinimo;
    }


    /**
     * Gets the pagoMinimoNoCubierto value for this ConsultaSaldoInfoDTO.
     * 
     * @return pagoMinimoNoCubierto
     */
    public java.lang.String getPagoMinimoNoCubierto() {
        return pagoMinimoNoCubierto;
    }


    /**
     * Sets the pagoMinimoNoCubierto value for this ConsultaSaldoInfoDTO.
     * 
     * @param pagoMinimoNoCubierto
     */
    public void setPagoMinimoNoCubierto(java.lang.String pagoMinimoNoCubierto) {
        this.pagoMinimoNoCubierto = pagoMinimoNoCubierto;
    }


    /**
     * Gets the saldoActual value for this ConsultaSaldoInfoDTO.
     * 
     * @return saldoActual
     */
    public java.lang.String getSaldoActual() {
        return saldoActual;
    }


    /**
     * Sets the saldoActual value for this ConsultaSaldoInfoDTO.
     * 
     * @param saldoActual
     */
    public void setSaldoActual(java.lang.String saldoActual) {
        this.saldoActual = saldoActual;
    }


    /**
     * Gets the tipoCuenta value for this ConsultaSaldoInfoDTO.
     * 
     * @return tipoCuenta
     */
    public java.lang.Short getTipoCuenta() {
        return tipoCuenta;
    }


    /**
     * Sets the tipoCuenta value for this ConsultaSaldoInfoDTO.
     * 
     * @param tipoCuenta
     */
    public void setTipoCuenta(java.lang.Short tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }


    /**
     * Gets the tipoDocumento value for this ConsultaSaldoInfoDTO.
     * 
     * @return tipoDocumento
     */
    public java.lang.Short getTipoDocumento() {
        return tipoDocumento;
    }


    /**
     * Sets the tipoDocumento value for this ConsultaSaldoInfoDTO.
     * 
     * @param tipoDocumento
     */
    public void setTipoDocumento(java.lang.Short tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConsultaSaldoInfoDTO)) return false;
        ConsultaSaldoInfoDTO other = (ConsultaSaldoInfoDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cantAuthPendientes==null && other.getCantAuthPendientes()==null) || 
             (this.cantAuthPendientes!=null &&
              this.cantAuthPendientes.equals(other.getCantAuthPendientes()))) &&
            ((this.cicloFacturacion==null && other.getCicloFacturacion()==null) || 
             (this.cicloFacturacion!=null &&
              this.cicloFacturacion.equals(other.getCicloFacturacion()))) &&
            ((this.codAgencia==null && other.getCodAgencia()==null) || 
             (this.codAgencia!=null &&
              this.codAgencia.equals(other.getCodAgencia()))) &&
            ((this.codEstadoCuenta==null && other.getCodEstadoCuenta()==null) || 
             (this.codEstadoCuenta!=null &&
              this.codEstadoCuenta.equals(other.getCodEstadoCuenta()))) &&
            ((this.codEstadoTarjeta==null && other.getCodEstadoTarjeta()==null) || 
             (this.codEstadoTarjeta!=null &&
              this.codEstadoTarjeta.equals(other.getCodEstadoTarjeta()))) &&
            ((this.codGrupoAfinidad==null && other.getCodGrupoAfinidad()==null) || 
             (this.codGrupoAfinidad!=null &&
              this.codGrupoAfinidad.equals(other.getCodGrupoAfinidad()))) &&
            ((this.codMarca==null && other.getCodMarca()==null) || 
             (this.codMarca!=null &&
              this.codMarca.equals(other.getCodMarca()))) &&
            ((this.codModeloLiquidacion==null && other.getCodModeloLiquidacion()==null) || 
             (this.codModeloLiquidacion!=null &&
              this.codModeloLiquidacion.equals(other.getCodModeloLiquidacion()))) &&
            ((this.codMoneda==null && other.getCodMoneda()==null) || 
             (this.codMoneda!=null &&
              this.codMoneda.equals(other.getCodMoneda()))) &&
            ((this.codProducto==null && other.getCodProducto()==null) || 
             (this.codProducto!=null &&
              this.codProducto.equals(other.getCodProducto()))) &&
            ((this.codSucursal==null && other.getCodSucursal()==null) || 
             (this.codSucursal!=null &&
              this.codSucursal.equals(other.getCodSucursal()))) &&
            ((this.creditoDisponible==null && other.getCreditoDisponible()==null) || 
             (this.creditoDisponible!=null &&
              this.creditoDisponible.equals(other.getCreditoDisponible()))) &&
            ((this.cuotas==null && other.getCuotas()==null) || 
             (this.cuotas!=null &&
              java.util.Arrays.equals(this.cuotas, other.getCuotas()))) &&
            ((this.descripcionProducto==null && other.getDescripcionProducto()==null) || 
             (this.descripcionProducto!=null &&
              this.descripcionProducto.equals(other.getDescripcionProducto()))) &&
            ((this.direccion==null && other.getDireccion()==null) || 
             (this.direccion!=null &&
              this.direccion.equals(other.getDireccion()))) &&
            ((this.fechaAltaCuenta==null && other.getFechaAltaCuenta()==null) || 
             (this.fechaAltaCuenta!=null &&
              this.fechaAltaCuenta.equals(other.getFechaAltaCuenta()))) &&
            ((this.fechaEstadoActual==null && other.getFechaEstadoActual()==null) || 
             (this.fechaEstadoActual!=null &&
              this.fechaEstadoActual.equals(other.getFechaEstadoActual()))) &&
            ((this.fechaProxCierre==null && other.getFechaProxCierre()==null) || 
             (this.fechaProxCierre!=null &&
              this.fechaProxCierre.equals(other.getFechaProxCierre()))) &&
            ((this.fechaProxVenPago==null && other.getFechaProxVenPago()==null) || 
             (this.fechaProxVenPago!=null &&
              this.fechaProxVenPago.equals(other.getFechaProxVenPago()))) &&
            ((this.fechaUltimoCierre==null && other.getFechaUltimoCierre()==null) || 
             (this.fechaUltimoCierre!=null &&
              this.fechaUltimoCierre.equals(other.getFechaUltimoCierre()))) &&
            ((this.fechaUltmoPago==null && other.getFechaUltmoPago()==null) || 
             (this.fechaUltmoPago!=null &&
              this.fechaUltmoPago.equals(other.getFechaUltmoPago()))) &&
            ((this.fechaVencimientoTarjeta==null && other.getFechaVencimientoTarjeta()==null) || 
             (this.fechaVencimientoTarjeta!=null &&
              this.fechaVencimientoTarjeta.equals(other.getFechaVencimientoTarjeta()))) &&
            ((this.formaPago==null && other.getFormaPago()==null) || 
             (this.formaPago!=null &&
              this.formaPago.equals(other.getFormaPago()))) &&
            ((this.importeTotalAuthPendientes==null && other.getImporteTotalAuthPendientes()==null) || 
             (this.importeTotalAuthPendientes!=null &&
              this.importeTotalAuthPendientes.equals(other.getImporteTotalAuthPendientes()))) &&
            ((this.limiteCredito==null && other.getLimiteCredito()==null) || 
             (this.limiteCredito!=null &&
              this.limiteCredito.equals(other.getLimiteCredito()))) &&
            ((this.localidad==null && other.getLocalidad()==null) || 
             (this.localidad!=null &&
              this.localidad.equals(other.getLocalidad()))) &&
            ((this.maxSobregiro==null && other.getMaxSobregiro()==null) || 
             (this.maxSobregiro!=null &&
              this.maxSobregiro.equals(other.getMaxSobregiro()))) &&
            ((this.nombreEmpresa==null && other.getNombreEmpresa()==null) || 
             (this.nombreEmpresa!=null &&
              this.nombreEmpresa.equals(other.getNombreEmpresa()))) &&
            ((this.nombreTitular==null && other.getNombreTitular()==null) || 
             (this.nombreTitular!=null &&
              this.nombreTitular.equals(other.getNombreTitular()))) &&
            ((this.nroCuenta==null && other.getNroCuenta()==null) || 
             (this.nroCuenta!=null &&
              this.nroCuenta.equals(other.getNroCuenta()))) &&
            ((this.nroDocSecundario==null && other.getNroDocSecundario()==null) || 
             (this.nroDocSecundario!=null &&
              this.nroDocSecundario.equals(other.getNroDocSecundario()))) &&
            ((this.nroDocumeto==null && other.getNroDocumeto()==null) || 
             (this.nroDocumeto!=null &&
              this.nroDocumeto.equals(other.getNroDocumeto()))) &&
            ((this.nroTarjeta==null && other.getNroTarjeta()==null) || 
             (this.nroTarjeta!=null &&
              this.nroTarjeta.equals(other.getNroTarjeta()))) &&
            ((this.numCuentaBancaria==null && other.getNumCuentaBancaria()==null) || 
             (this.numCuentaBancaria!=null &&
              this.numCuentaBancaria.equals(other.getNumCuentaBancaria()))) &&
            ((this.pagoMinimo==null && other.getPagoMinimo()==null) || 
             (this.pagoMinimo!=null &&
              this.pagoMinimo.equals(other.getPagoMinimo()))) &&
            ((this.pagoMinimoNoCubierto==null && other.getPagoMinimoNoCubierto()==null) || 
             (this.pagoMinimoNoCubierto!=null &&
              this.pagoMinimoNoCubierto.equals(other.getPagoMinimoNoCubierto()))) &&
            ((this.saldoActual==null && other.getSaldoActual()==null) || 
             (this.saldoActual!=null &&
              this.saldoActual.equals(other.getSaldoActual()))) &&
            ((this.tipoCuenta==null && other.getTipoCuenta()==null) || 
             (this.tipoCuenta!=null &&
              this.tipoCuenta.equals(other.getTipoCuenta()))) &&
            ((this.tipoDocumento==null && other.getTipoDocumento()==null) || 
             (this.tipoDocumento!=null &&
              this.tipoDocumento.equals(other.getTipoDocumento())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCantAuthPendientes() != null) {
            _hashCode += getCantAuthPendientes().hashCode();
        }
        if (getCicloFacturacion() != null) {
            _hashCode += getCicloFacturacion().hashCode();
        }
        if (getCodAgencia() != null) {
            _hashCode += getCodAgencia().hashCode();
        }
        if (getCodEstadoCuenta() != null) {
            _hashCode += getCodEstadoCuenta().hashCode();
        }
        if (getCodEstadoTarjeta() != null) {
            _hashCode += getCodEstadoTarjeta().hashCode();
        }
        if (getCodGrupoAfinidad() != null) {
            _hashCode += getCodGrupoAfinidad().hashCode();
        }
        if (getCodMarca() != null) {
            _hashCode += getCodMarca().hashCode();
        }
        if (getCodModeloLiquidacion() != null) {
            _hashCode += getCodModeloLiquidacion().hashCode();
        }
        if (getCodMoneda() != null) {
            _hashCode += getCodMoneda().hashCode();
        }
        if (getCodProducto() != null) {
            _hashCode += getCodProducto().hashCode();
        }
        if (getCodSucursal() != null) {
            _hashCode += getCodSucursal().hashCode();
        }
        if (getCreditoDisponible() != null) {
            _hashCode += getCreditoDisponible().hashCode();
        }
        if (getCuotas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCuotas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCuotas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDescripcionProducto() != null) {
            _hashCode += getDescripcionProducto().hashCode();
        }
        if (getDireccion() != null) {
            _hashCode += getDireccion().hashCode();
        }
        if (getFechaAltaCuenta() != null) {
            _hashCode += getFechaAltaCuenta().hashCode();
        }
        if (getFechaEstadoActual() != null) {
            _hashCode += getFechaEstadoActual().hashCode();
        }
        if (getFechaProxCierre() != null) {
            _hashCode += getFechaProxCierre().hashCode();
        }
        if (getFechaProxVenPago() != null) {
            _hashCode += getFechaProxVenPago().hashCode();
        }
        if (getFechaUltimoCierre() != null) {
            _hashCode += getFechaUltimoCierre().hashCode();
        }
        if (getFechaUltmoPago() != null) {
            _hashCode += getFechaUltmoPago().hashCode();
        }
        if (getFechaVencimientoTarjeta() != null) {
            _hashCode += getFechaVencimientoTarjeta().hashCode();
        }
        if (getFormaPago() != null) {
            _hashCode += getFormaPago().hashCode();
        }
        if (getImporteTotalAuthPendientes() != null) {
            _hashCode += getImporteTotalAuthPendientes().hashCode();
        }
        if (getLimiteCredito() != null) {
            _hashCode += getLimiteCredito().hashCode();
        }
        if (getLocalidad() != null) {
            _hashCode += getLocalidad().hashCode();
        }
        if (getMaxSobregiro() != null) {
            _hashCode += getMaxSobregiro().hashCode();
        }
        if (getNombreEmpresa() != null) {
            _hashCode += getNombreEmpresa().hashCode();
        }
        if (getNombreTitular() != null) {
            _hashCode += getNombreTitular().hashCode();
        }
        if (getNroCuenta() != null) {
            _hashCode += getNroCuenta().hashCode();
        }
        if (getNroDocSecundario() != null) {
            _hashCode += getNroDocSecundario().hashCode();
        }
        if (getNroDocumeto() != null) {
            _hashCode += getNroDocumeto().hashCode();
        }
        if (getNroTarjeta() != null) {
            _hashCode += getNroTarjeta().hashCode();
        }
        if (getNumCuentaBancaria() != null) {
            _hashCode += getNumCuentaBancaria().hashCode();
        }
        if (getPagoMinimo() != null) {
            _hashCode += getPagoMinimo().hashCode();
        }
        if (getPagoMinimoNoCubierto() != null) {
            _hashCode += getPagoMinimoNoCubierto().hashCode();
        }
        if (getSaldoActual() != null) {
            _hashCode += getSaldoActual().hashCode();
        }
        if (getTipoCuenta() != null) {
            _hashCode += getTipoCuenta().hashCode();
        }
        if (getTipoDocumento() != null) {
            _hashCode += getTipoDocumento().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConsultaSaldoInfoDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "ConsultaSaldoInfoDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cantAuthPendientes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CantAuthPendientes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cicloFacturacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CicloFacturacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAgencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodAgencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEstadoCuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodEstadoCuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEstadoTarjeta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodEstadoTarjeta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codGrupoAfinidad");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodGrupoAfinidad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMarca");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodMarca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codModeloLiquidacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodModeloLiquidacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMoneda");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodMoneda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codProducto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodProducto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSucursal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodSucursal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditoDisponible");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CreditoDisponible"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cuotas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "Cuotas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CuotasDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CuotasDTO"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcionProducto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "DescripcionProducto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("direccion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "Direccion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaAltaCuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "FechaAltaCuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaEstadoActual");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "FechaEstadoActual"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaProxCierre");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "FechaProxCierre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaProxVenPago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "FechaProxVenPago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaUltimoCierre");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "FechaUltimoCierre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaUltmoPago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "FechaUltmoPago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaVencimientoTarjeta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "FechaVencimientoTarjeta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("formaPago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "FormaPago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("importeTotalAuthPendientes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "ImporteTotalAuthPendientes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limiteCredito");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "LimiteCredito"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("localidad");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "Localidad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxSobregiro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "MaxSobregiro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombreEmpresa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "NombreEmpresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombreTitular");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "NombreTitular"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroCuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "NroCuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroDocSecundario");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "NroDocSecundario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroDocumeto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "NroDocumeto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroTarjeta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "NroTarjeta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCuentaBancaria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "NumCuentaBancaria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagoMinimo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "PagoMinimo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagoMinimoNoCubierto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "PagoMinimoNoCubierto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saldoActual");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "SaldoActual"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoCuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "TipoCuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoDocumento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "TipoDocumento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
