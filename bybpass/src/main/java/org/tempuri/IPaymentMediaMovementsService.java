/**
 * IPaymentMediaMovementsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface IPaymentMediaMovementsService extends java.rmi.Remote {
    public org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.RespuestaConsultaMovimientosDTO getMovementInfo(org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.ConsultaMovTrxDTO request) throws java.rmi.RemoteException;
    public org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance.RespuestaConsultaSaldoDTO getBalanceInfo(org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance.ConsultaSaldoDTO request) throws java.rmi.RemoteException;
}
