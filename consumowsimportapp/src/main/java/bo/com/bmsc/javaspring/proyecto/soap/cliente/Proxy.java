package bo.com.bmsc.javaspring.proyecto.soap.cliente;

import java.net.MalformedURLException;
import java.net.URL;

public class Proxy {

    private String urlWsdl;
    private ApplicationPort proxy;
    private ObjectFactory objectFactory;

    public Proxy(String urlWsdl) throws MalformedURLException {
        this.urlWsdl = urlWsdl;
        this.objectFactory = new ObjectFactory();
        this.proxy = new ApplicationPortService(
                new URL(this.urlWsdl)).getApplicationPortSoap11();
    }

    public ListarClientesResponse listarClientes(){
        ListarClientesRequest request = objectFactory.createListarClientesRequest();
        return proxy.listarClientes(request);
    }

    public ClienteDTO crearClientes(ClienteDTO clienteDTO){
        CrearClientesRequest crearClientesRequest = objectFactory.createCrearClientesRequest();
        crearClientesRequest.setCliente(clienteDTO);
        proxy.crearClientes(crearClientesRequest);
        return clienteDTO;
    }



}
