package bo.com.bmsc.javaspring.proyecto.soap.cliente;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DemoCrear {

//    public static void main(String[] args) throws MalformedURLException, DatatypeConfigurationException {
//        ApplicationPort proxy = new ApplicationPortService(
//                new URL("http://localhost:8080/ws/application.wsdl")).getApplicationPortSoap11();
//        ObjectFactory objectFactory = new ObjectFactory();
//        CrearClientesRequest request = objectFactory.createCrearClientesRequest();
//        ClienteDTO clienteDTO = objectFactory.createClienteDTO();
//        clienteDTO.setNombre("Nombre wsimport");
//        clienteDTO.setApellido("Apellido wsimport");
//        clienteDTO.setCi("CI wsimport");
//        clienteDTO.setCodigo("COD wsimport");
//        GregorianCalendar calendar = new GregorianCalendar();
//        calendar.set(2021, Calendar.JANUARY, 10);
//        clienteDTO.setFechaNacimiento(DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar));
//        request.setCliente(clienteDTO);
//        proxy.crearClientes(request);
//    }

    public static void main(String[] args) throws MalformedURLException, DatatypeConfigurationException {
        Proxy proxy = new Proxy("http://localhost:8080/ws/application.wsdl");
        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO.setNombre("Nombre wsimport 2");
        clienteDTO.setApellido("Apellido wsimport 2");
        clienteDTO.setCi("CI wsimport 2");
        clienteDTO.setCodigo("COD wsimport 2");
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(2021, Calendar.JANUARY, 10);
        clienteDTO.setFechaNacimiento(DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar));
        proxy.crearClientes(clienteDTO);
    }

}
