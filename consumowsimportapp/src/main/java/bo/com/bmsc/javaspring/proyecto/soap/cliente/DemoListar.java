package bo.com.bmsc.javaspring.proyecto.soap.cliente;

import java.net.MalformedURLException;
import java.net.URL;

public class DemoListar {

    public static void main(String[] args) throws MalformedURLException {
        ApplicationPort proxy = new ApplicationPortService(
                new URL("http://localhost:8080/ws/application.wsdl")).getApplicationPortSoap11();
        ObjectFactory objectFactory = new ObjectFactory();
        ListarClientesRequest request = objectFactory.createListarClientesRequest();
        ListarClientesResponse response = proxy.listarClientes(request);
        for(ClienteDTO cliente: response.getClientes()){
            System.out.println("Cliente: " + cliente.getId() + ", " + cliente.getNombre() + ", " +
                    cliente.getApellido() + ", " + cliente.getFechaNacimiento());
        }
    }

}
