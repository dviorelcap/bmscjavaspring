
package bo.com.bmsc.javaspring.proyecto.soap.cliente;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the bo.com.bmsc.javaspring.proyecto.soap.cliente package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListarClientesResponse_QNAME = new QName("http://bmsc.com.bo/javaspring/proyecto/soap/ws", "listarClientesResponse");
    private final static QName _ClienteDTO_QNAME = new QName("http://bmsc.com.bo/javaspring/proyecto/soap/ws", "clienteDTO");
    private final static QName _CrearClientesRequest_QNAME = new QName("http://bmsc.com.bo/javaspring/proyecto/soap/ws", "crearClientesRequest");
    private final static QName _ListarClientesRequest_QNAME = new QName("http://bmsc.com.bo/javaspring/proyecto/soap/ws", "listarClientesRequest");
    private final static QName _CrearClientesResponse_QNAME = new QName("http://bmsc.com.bo/javaspring/proyecto/soap/ws", "crearClientesResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: bo.com.bmsc.javaspring.proyecto.soap.cliente
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CrearClientesResponse }
     * 
     */
    public CrearClientesResponse createCrearClientesResponse() {
        return new CrearClientesResponse();
    }

    /**
     * Create an instance of {@link ClienteDTO }
     * 
     */
    public ClienteDTO createClienteDTO() {
        return new ClienteDTO();
    }

    /**
     * Create an instance of {@link ListarClientesResponse }
     * 
     */
    public ListarClientesResponse createListarClientesResponse() {
        return new ListarClientesResponse();
    }

    /**
     * Create an instance of {@link ListarClientesRequest }
     * 
     */
    public ListarClientesRequest createListarClientesRequest() {
        return new ListarClientesRequest();
    }

    /**
     * Create an instance of {@link CrearClientesRequest }
     * 
     */
    public CrearClientesRequest createCrearClientesRequest() {
        return new CrearClientesRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarClientesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bmsc.com.bo/javaspring/proyecto/soap/ws", name = "listarClientesResponse")
    public JAXBElement<ListarClientesResponse> createListarClientesResponse(ListarClientesResponse value) {
        return new JAXBElement<ListarClientesResponse>(_ListarClientesResponse_QNAME, ListarClientesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClienteDTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bmsc.com.bo/javaspring/proyecto/soap/ws", name = "clienteDTO")
    public JAXBElement<ClienteDTO> createClienteDTO(ClienteDTO value) {
        return new JAXBElement<ClienteDTO>(_ClienteDTO_QNAME, ClienteDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearClientesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bmsc.com.bo/javaspring/proyecto/soap/ws", name = "crearClientesRequest")
    public JAXBElement<CrearClientesRequest> createCrearClientesRequest(CrearClientesRequest value) {
        return new JAXBElement<CrearClientesRequest>(_CrearClientesRequest_QNAME, CrearClientesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarClientesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bmsc.com.bo/javaspring/proyecto/soap/ws", name = "listarClientesRequest")
    public JAXBElement<ListarClientesRequest> createListarClientesRequest(ListarClientesRequest value) {
        return new JAXBElement<ListarClientesRequest>(_ListarClientesRequest_QNAME, ListarClientesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearClientesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bmsc.com.bo/javaspring/proyecto/soap/ws", name = "crearClientesResponse")
    public JAXBElement<CrearClientesResponse> createCrearClientesResponse(CrearClientesResponse value) {
        return new JAXBElement<CrearClientesResponse>(_CrearClientesResponse_QNAME, CrearClientesResponse.class, null, value);
    }

}
