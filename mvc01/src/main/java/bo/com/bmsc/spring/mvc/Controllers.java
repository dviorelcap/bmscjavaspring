package bo.com.bmsc.spring.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

@Controller
public class Controllers {


    @RequestMapping("/hola")
    public String hola(){
        return "hola";
    }

    @RequestMapping("/mundo")
    public String mundo(){
        return "mundo";
    }

    @RequestMapping("/mensaje")
    public String mensaje(ModelMap model){
        model.addAttribute("mensaje",
                "Mensaje desde Spring MVC, son las " + new Date());
        return "mensaje";
    }

    @RequestMapping("/mensaje2")
    public ModelAndView mensaje2(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("mensaje");
        modelAndView.addObject("mensaje",
                "Otro mensaje desde MVC, son las " + new Date());
        return modelAndView;
    }

}
