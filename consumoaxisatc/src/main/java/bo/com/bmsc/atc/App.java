package bo.com.bmsc.atc;

import org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance.CabeceraInfoDTO;
import org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance.ConsultaSaldoInfoDTO;
import org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance.RespuestaConsultaSaldoDTO;
import org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.MovTrxInfoDTO;
import org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.RespuestaConsultaMovimientosDTO;

import javax.net.ssl.*;
import javax.xml.rpc.ServiceException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Scanner;

public class App {

    static {
        disableSslVerification();
    }

    private static void disableSslVerification() {
        try
        {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws MalformedURLException, NoSuchAlgorithmException, KeyManagementException, ServiceException, RemoteException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese url (no valida SSL)");
        String url = scanner.nextLine();
        System.out.println("Seleccione opcion\n1: getBalanceInfo\n2: getMovementInfo");
        int opcion = scanner.nextInt();
        String discarded = scanner.nextLine();

        // Create a context that doesn't check certificates.
        Proxy proxy = new Proxy(url);

        String codigoBanco = "";
        String codigoMatriculaSolicitante = "";
        String numeroOperacion = "";
        String codigoTerminalCanal = "";
        String numeroTarjeta = "";
        String codigoUsuarioAplicativo = "";

        switch (opcion){
            case 1:
                System.out.println("Ingrese codigoBanco:");
                codigoBanco = scanner.nextLine();
                System.out.println("Ingrese codigoMatriculaSolicitante:");
                codigoMatriculaSolicitante = scanner.nextLine();
                System.out.println("Ingrese numeroOperacion:");
                numeroOperacion = scanner.nextLine();
                System.out.println("Ingrese codigoTerminalCanal:");
                codigoTerminalCanal = scanner.nextLine();
                System.out.println("Ingrese numeroTarjeta:");
                numeroTarjeta = scanner.nextLine();
                System.out.println("Ingrese codigoUsuarioAplicativo:");
                codigoUsuarioAplicativo = scanner.nextLine();
                RespuestaConsultaSaldoDTO balanceInfo = proxy.getBalanceInfo(codigoBanco, codigoMatriculaSolicitante,
                        numeroOperacion, codigoTerminalCanal, numeroTarjeta, codigoUsuarioAplicativo);
                System.out.println("Respuesta:");
                if (balanceInfo.getHeader() != null){
                    CabeceraInfoDTO cabeceraInfoDTO = balanceInfo.getHeader();
                    if (cabeceraInfoDTO != null){
                        System.out.println("Header");
                        System.out.println("codigoBanco: " + cabeceraInfoDTO.getCodigoBanco());
                        System.out.println("codigoMatriculaSolicitante: " + cabeceraInfoDTO.getCodigoMatriculaSolicitante());
                        System.out.println("codigoTerminalCanal: " + cabeceraInfoDTO.getCodigoTerminalCanal());
                        System.out.println("codigoUsuarioAplicativo: " + cabeceraInfoDTO.getCodigoUsuarioAplicativo());
                        System.out.println("numeroOperacion: " + cabeceraInfoDTO.getNumeroOperacion());
                    }
                }
                if (balanceInfo.getConsultaSaldoInfo() != null){
                    ConsultaSaldoInfoDTO consultaSaldoInfoDTO = balanceInfo.getConsultaSaldoInfo();
                    if (consultaSaldoInfoDTO != null){
                        System.out.println("Value");
                        System.out.println("cantAuthPendientes: " + consultaSaldoInfoDTO.getCantAuthPendientes());
                        System.out.println("cicloFacturacion: " + consultaSaldoInfoDTO.getCicloFacturacion());
                        System.out.println("codAgencia: " + consultaSaldoInfoDTO.getCodAgencia());
                        System.out.println("codEstadoCuenta: " + consultaSaldoInfoDTO.getCodEstadoCuenta());
                        System.out.println("codMarca: " + consultaSaldoInfoDTO.getCodMarca());
                        System.out.println("codEstadoTarjeta: " + consultaSaldoInfoDTO.getCodEstadoTarjeta());
                        System.out.println("codGrupoAfinidad: " + consultaSaldoInfoDTO.getCodGrupoAfinidad());
                        System.out.println("codModeloLiquidacion: " + consultaSaldoInfoDTO.getCodModeloLiquidacion());
                        System.out.println("codMoneda: " + consultaSaldoInfoDTO.getCodMoneda());
                        System.out.println("codProducto: " + consultaSaldoInfoDTO.getCodProducto());
                        System.out.println("codSucursal: " + consultaSaldoInfoDTO.getCodSucursal());
                        System.out.println("creditoDisponible: " + consultaSaldoInfoDTO.getCreditoDisponible());
                        System.out.println("descripcionProducto: " + consultaSaldoInfoDTO.getDescripcionProducto());
                        System.out.println("direccion: " + consultaSaldoInfoDTO.getDireccion());
                        System.out.println("cantidad cuotas: " + consultaSaldoInfoDTO.getCuotas().length);
                    }
                }
                break;
            case 2:
                System.out.println("Ingrese codigoBanco:");
                codigoBanco = scanner.nextLine();
                System.out.println("Ingrese numeroOperacion:");
                numeroOperacion = scanner.nextLine();
                System.out.println("Ingrese codigoTerminalCanal:");
                codigoTerminalCanal = scanner.nextLine();
                System.out.println("Ingrese numeroTarjeta:");
                numeroTarjeta = scanner.nextLine();
                System.out.println("Ingrese codigoUsuarioAplicativo:");
                codigoUsuarioAplicativo = scanner.nextLine();
                System.out.println("Ingrese tipoMovimiento:");
                String tipoMovimiento = scanner.nextLine();
                System.out.println("Ingrese cantidadMovimientos:");
                Integer cantidadMovimientos = scanner.nextInt();
                RespuestaConsultaMovimientosDTO movementInfo = proxy.getMovementInfo(codigoBanco,
                        numeroOperacion,
                        codigoTerminalCanal,
                        numeroTarjeta,
                        codigoUsuarioAplicativo,
                        tipoMovimiento,
                        cantidadMovimientos);
                System.out.println("Respuesta:");
                if (movementInfo.getHeader() != null){
                    org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.CabeceraInfoDTO cabeceraInfoDTO2 = movementInfo.getHeader();
                    if (cabeceraInfoDTO2 != null){
                        System.out.println("Header");
                        System.out.println("codigoTerminalCanal: " + cabeceraInfoDTO2.getCodigoTerminalCanal());
                        System.out.println("codigoUsuarioAplicativo: " + cabeceraInfoDTO2.getCodigoUsuarioAplicativo());
                        System.out.println("numeroOperacion: " + cabeceraInfoDTO2.getNumeroOperacion());
                    }
                }
                if (movementInfo.getConsultaMovimientosInfo() != null){
                    MovTrxInfoDTO[] consultaMovimientosInfoDTO = movementInfo.getConsultaMovimientosInfo();
                    if (consultaMovimientosInfoDTO != null){
                        System.out.println("Value");
                        //List<MovTrxInfoDTO> movTrxInfoDTOs = consultaMovimientosInfoDTO;
                        //for (MovTrxInfoDTO movTrxInfoDTO: movTrxInfoDTOs){
                        for (MovTrxInfoDTO movTrxInfoDTO: consultaMovimientosInfoDTO){
                            System.out.println("cuota: " + movTrxInfoDTO.getCuota());
                            System.out.println("cantidadPuntos: " + movTrxInfoDTO.getCantidadPuntos());
                            System.out.println("codigoMoneda: " + movTrxInfoDTO.getCodigoMoneda());
                            System.out.println("numeroTarjeta: " + movTrxInfoDTO.getNumeroTarjeta());
                        }
                    }
                }
                break;
            default:
                System.out.println("Termina la ejecucion con opcion invalida");
                break;
        }
    }

}
