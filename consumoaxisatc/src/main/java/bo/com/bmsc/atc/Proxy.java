package bo.com.bmsc.atc;

import org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance.ConsultaSaldoDTO;
import org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance.RespuestaConsultaSaldoDTO;
import org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.ConsultaMovTrxDTO;
import org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements.RespuestaConsultaMovimientosDTO;
import org.tempuri.IPaymentMediaMovementsService;
import org.tempuri.PaymentMediaMovementsServiceLocator;

import javax.xml.rpc.ServiceException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

public class Proxy {

    private IPaymentMediaMovementsService proxy;

    public Proxy(String url) throws MalformedURLException, ServiceException {
        proxy = new PaymentMediaMovementsServiceLocator()
                        .getBasicHttpBinding_IPaymentMediaMovementsService(new URL(url));
    }

    public RespuestaConsultaSaldoDTO getBalanceInfo(String codigoBanco, String codigoMatriculaSolicitante,
                                 String numeroOperacion, String codigoTerminalCanal,
                                 String numeroTarjeta, String codigoUsuarioAplicativo) throws RemoteException {
        ConsultaSaldoDTO consultaSaldoDTO = new ConsultaSaldoDTO();
        consultaSaldoDTO.setCodigoBanco(codigoBanco);
        consultaSaldoDTO.setCodigoMatriculaSolicitante(codigoMatriculaSolicitante);
        consultaSaldoDTO.setNumeroOperacion(numeroOperacion);
        consultaSaldoDTO.setCodigoTerminalCanal(codigoTerminalCanal);
        consultaSaldoDTO.setNumeroTarjeta(numeroTarjeta);
        consultaSaldoDTO.setCodigoUsuarioAplicativo(codigoUsuarioAplicativo);
        RespuestaConsultaSaldoDTO balanceInfo = proxy.getBalanceInfo(consultaSaldoDTO);
        return balanceInfo;
    }

    public RespuestaConsultaMovimientosDTO getMovementInfo(String codigoBanco, String numeroOperacion,
                                  String codigoTerminalCanal, String numeroTarjeta,
                                  String codigoUsuarioAplicativo, String tipoMovimiento,
                                  Integer cantidadMovimientos) throws RemoteException {
        ConsultaMovTrxDTO consultaMovTrxDTO = new ConsultaMovTrxDTO();
        consultaMovTrxDTO.setCodigoBanco(codigoBanco);
        consultaMovTrxDTO.setNumeroOperacion(numeroOperacion);
        consultaMovTrxDTO.setCodigoTerminalCanal(codigoTerminalCanal);
        consultaMovTrxDTO.setNumeroTarjeta(numeroTarjeta);
        consultaMovTrxDTO.setCodigoUsuarioAplicativo(codigoUsuarioAplicativo);
        consultaMovTrxDTO.setTipoMovimiento(tipoMovimiento);
        consultaMovTrxDTO.setCantidadMovimientos(cantidadMovimientos);
        RespuestaConsultaMovimientosDTO movementInfo = proxy.getMovementInfo(consultaMovTrxDTO);
        return movementInfo;
    }

}
