/**
 * MovTrxInfoDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements;

public class MovTrxInfoDTO  implements java.io.Serializable {
    private String cantidadPuntos;

    private String codCatBalance;

    private String codCatEECC;

    private String codCatFin;

    private String codProgramaFidelidad;

    private String codigoAutorizacion;

    private Short codigoCategoriaComercio;

    private String codigoMoneda;

    private String cuota;

    private String descripcionMovimiento;

    private String diasFaltantes;

    private Integer fechaOrigen;

    private Integer fechaPosteo;

    private String flagFacturado;

    private Long idMovimientoTransaccion;

    private String importe;

    private String numeroTarjeta;

    private String tipoMovimiento;

    private Short tipoRelacionFinanciera;

    public MovTrxInfoDTO() {
    }

    public MovTrxInfoDTO(
           String cantidadPuntos,
           String codCatBalance,
           String codCatEECC,
           String codCatFin,
           String codProgramaFidelidad,
           String codigoAutorizacion,
           Short codigoCategoriaComercio,
           String codigoMoneda,
           String cuota,
           String descripcionMovimiento,
           String diasFaltantes,
           Integer fechaOrigen,
           Integer fechaPosteo,
           String flagFacturado,
           Long idMovimientoTransaccion,
           String importe,
           String numeroTarjeta,
           String tipoMovimiento,
           Short tipoRelacionFinanciera) {
           this.cantidadPuntos = cantidadPuntos;
           this.codCatBalance = codCatBalance;
           this.codCatEECC = codCatEECC;
           this.codCatFin = codCatFin;
           this.codProgramaFidelidad = codProgramaFidelidad;
           this.codigoAutorizacion = codigoAutorizacion;
           this.codigoCategoriaComercio = codigoCategoriaComercio;
           this.codigoMoneda = codigoMoneda;
           this.cuota = cuota;
           this.descripcionMovimiento = descripcionMovimiento;
           this.diasFaltantes = diasFaltantes;
           this.fechaOrigen = fechaOrigen;
           this.fechaPosteo = fechaPosteo;
           this.flagFacturado = flagFacturado;
           this.idMovimientoTransaccion = idMovimientoTransaccion;
           this.importe = importe;
           this.numeroTarjeta = numeroTarjeta;
           this.tipoMovimiento = tipoMovimiento;
           this.tipoRelacionFinanciera = tipoRelacionFinanciera;
    }


    /**
     * Gets the cantidadPuntos value for this MovTrxInfoDTO.
     * 
     * @return cantidadPuntos
     */
    public String getCantidadPuntos() {
        return cantidadPuntos;
    }


    /**
     * Sets the cantidadPuntos value for this MovTrxInfoDTO.
     * 
     * @param cantidadPuntos
     */
    public void setCantidadPuntos(String cantidadPuntos) {
        this.cantidadPuntos = cantidadPuntos;
    }


    /**
     * Gets the codCatBalance value for this MovTrxInfoDTO.
     * 
     * @return codCatBalance
     */
    public String getCodCatBalance() {
        return codCatBalance;
    }


    /**
     * Sets the codCatBalance value for this MovTrxInfoDTO.
     * 
     * @param codCatBalance
     */
    public void setCodCatBalance(String codCatBalance) {
        this.codCatBalance = codCatBalance;
    }


    /**
     * Gets the codCatEECC value for this MovTrxInfoDTO.
     * 
     * @return codCatEECC
     */
    public String getCodCatEECC() {
        return codCatEECC;
    }


    /**
     * Sets the codCatEECC value for this MovTrxInfoDTO.
     * 
     * @param codCatEECC
     */
    public void setCodCatEECC(String codCatEECC) {
        this.codCatEECC = codCatEECC;
    }


    /**
     * Gets the codCatFin value for this MovTrxInfoDTO.
     * 
     * @return codCatFin
     */
    public String getCodCatFin() {
        return codCatFin;
    }


    /**
     * Sets the codCatFin value for this MovTrxInfoDTO.
     * 
     * @param codCatFin
     */
    public void setCodCatFin(String codCatFin) {
        this.codCatFin = codCatFin;
    }


    /**
     * Gets the codProgramaFidelidad value for this MovTrxInfoDTO.
     * 
     * @return codProgramaFidelidad
     */
    public String getCodProgramaFidelidad() {
        return codProgramaFidelidad;
    }


    /**
     * Sets the codProgramaFidelidad value for this MovTrxInfoDTO.
     * 
     * @param codProgramaFidelidad
     */
    public void setCodProgramaFidelidad(String codProgramaFidelidad) {
        this.codProgramaFidelidad = codProgramaFidelidad;
    }


    /**
     * Gets the codigoAutorizacion value for this MovTrxInfoDTO.
     * 
     * @return codigoAutorizacion
     */
    public String getCodigoAutorizacion() {
        return codigoAutorizacion;
    }


    /**
     * Sets the codigoAutorizacion value for this MovTrxInfoDTO.
     * 
     * @param codigoAutorizacion
     */
    public void setCodigoAutorizacion(String codigoAutorizacion) {
        this.codigoAutorizacion = codigoAutorizacion;
    }


    /**
     * Gets the codigoCategoriaComercio value for this MovTrxInfoDTO.
     * 
     * @return codigoCategoriaComercio
     */
    public Short getCodigoCategoriaComercio() {
        return codigoCategoriaComercio;
    }


    /**
     * Sets the codigoCategoriaComercio value for this MovTrxInfoDTO.
     * 
     * @param codigoCategoriaComercio
     */
    public void setCodigoCategoriaComercio(Short codigoCategoriaComercio) {
        this.codigoCategoriaComercio = codigoCategoriaComercio;
    }


    /**
     * Gets the codigoMoneda value for this MovTrxInfoDTO.
     * 
     * @return codigoMoneda
     */
    public String getCodigoMoneda() {
        return codigoMoneda;
    }


    /**
     * Sets the codigoMoneda value for this MovTrxInfoDTO.
     * 
     * @param codigoMoneda
     */
    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }


    /**
     * Gets the cuota value for this MovTrxInfoDTO.
     * 
     * @return cuota
     */
    public String getCuota() {
        return cuota;
    }


    /**
     * Sets the cuota value for this MovTrxInfoDTO.
     * 
     * @param cuota
     */
    public void setCuota(String cuota) {
        this.cuota = cuota;
    }


    /**
     * Gets the descripcionMovimiento value for this MovTrxInfoDTO.
     * 
     * @return descripcionMovimiento
     */
    public String getDescripcionMovimiento() {
        return descripcionMovimiento;
    }


    /**
     * Sets the descripcionMovimiento value for this MovTrxInfoDTO.
     * 
     * @param descripcionMovimiento
     */
    public void setDescripcionMovimiento(String descripcionMovimiento) {
        this.descripcionMovimiento = descripcionMovimiento;
    }


    /**
     * Gets the diasFaltantes value for this MovTrxInfoDTO.
     * 
     * @return diasFaltantes
     */
    public String getDiasFaltantes() {
        return diasFaltantes;
    }


    /**
     * Sets the diasFaltantes value for this MovTrxInfoDTO.
     * 
     * @param diasFaltantes
     */
    public void setDiasFaltantes(String diasFaltantes) {
        this.diasFaltantes = diasFaltantes;
    }


    /**
     * Gets the fechaOrigen value for this MovTrxInfoDTO.
     * 
     * @return fechaOrigen
     */
    public Integer getFechaOrigen() {
        return fechaOrigen;
    }


    /**
     * Sets the fechaOrigen value for this MovTrxInfoDTO.
     * 
     * @param fechaOrigen
     */
    public void setFechaOrigen(Integer fechaOrigen) {
        this.fechaOrigen = fechaOrigen;
    }


    /**
     * Gets the fechaPosteo value for this MovTrxInfoDTO.
     * 
     * @return fechaPosteo
     */
    public Integer getFechaPosteo() {
        return fechaPosteo;
    }


    /**
     * Sets the fechaPosteo value for this MovTrxInfoDTO.
     * 
     * @param fechaPosteo
     */
    public void setFechaPosteo(Integer fechaPosteo) {
        this.fechaPosteo = fechaPosteo;
    }


    /**
     * Gets the flagFacturado value for this MovTrxInfoDTO.
     * 
     * @return flagFacturado
     */
    public String getFlagFacturado() {
        return flagFacturado;
    }


    /**
     * Sets the flagFacturado value for this MovTrxInfoDTO.
     * 
     * @param flagFacturado
     */
    public void setFlagFacturado(String flagFacturado) {
        this.flagFacturado = flagFacturado;
    }


    /**
     * Gets the idMovimientoTransaccion value for this MovTrxInfoDTO.
     * 
     * @return idMovimientoTransaccion
     */
    public Long getIdMovimientoTransaccion() {
        return idMovimientoTransaccion;
    }


    /**
     * Sets the idMovimientoTransaccion value for this MovTrxInfoDTO.
     * 
     * @param idMovimientoTransaccion
     */
    public void setIdMovimientoTransaccion(Long idMovimientoTransaccion) {
        this.idMovimientoTransaccion = idMovimientoTransaccion;
    }


    /**
     * Gets the importe value for this MovTrxInfoDTO.
     * 
     * @return importe
     */
    public String getImporte() {
        return importe;
    }


    /**
     * Sets the importe value for this MovTrxInfoDTO.
     * 
     * @param importe
     */
    public void setImporte(String importe) {
        this.importe = importe;
    }


    /**
     * Gets the numeroTarjeta value for this MovTrxInfoDTO.
     * 
     * @return numeroTarjeta
     */
    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }


    /**
     * Sets the numeroTarjeta value for this MovTrxInfoDTO.
     * 
     * @param numeroTarjeta
     */
    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }


    /**
     * Gets the tipoMovimiento value for this MovTrxInfoDTO.
     * 
     * @return tipoMovimiento
     */
    public String getTipoMovimiento() {
        return tipoMovimiento;
    }


    /**
     * Sets the tipoMovimiento value for this MovTrxInfoDTO.
     * 
     * @param tipoMovimiento
     */
    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }


    /**
     * Gets the tipoRelacionFinanciera value for this MovTrxInfoDTO.
     * 
     * @return tipoRelacionFinanciera
     */
    public Short getTipoRelacionFinanciera() {
        return tipoRelacionFinanciera;
    }


    /**
     * Sets the tipoRelacionFinanciera value for this MovTrxInfoDTO.
     * 
     * @param tipoRelacionFinanciera
     */
    public void setTipoRelacionFinanciera(Short tipoRelacionFinanciera) {
        this.tipoRelacionFinanciera = tipoRelacionFinanciera;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof MovTrxInfoDTO)) return false;
        MovTrxInfoDTO other = (MovTrxInfoDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cantidadPuntos==null && other.getCantidadPuntos()==null) || 
             (this.cantidadPuntos!=null &&
              this.cantidadPuntos.equals(other.getCantidadPuntos()))) &&
            ((this.codCatBalance==null && other.getCodCatBalance()==null) || 
             (this.codCatBalance!=null &&
              this.codCatBalance.equals(other.getCodCatBalance()))) &&
            ((this.codCatEECC==null && other.getCodCatEECC()==null) || 
             (this.codCatEECC!=null &&
              this.codCatEECC.equals(other.getCodCatEECC()))) &&
            ((this.codCatFin==null && other.getCodCatFin()==null) || 
             (this.codCatFin!=null &&
              this.codCatFin.equals(other.getCodCatFin()))) &&
            ((this.codProgramaFidelidad==null && other.getCodProgramaFidelidad()==null) || 
             (this.codProgramaFidelidad!=null &&
              this.codProgramaFidelidad.equals(other.getCodProgramaFidelidad()))) &&
            ((this.codigoAutorizacion==null && other.getCodigoAutorizacion()==null) || 
             (this.codigoAutorizacion!=null &&
              this.codigoAutorizacion.equals(other.getCodigoAutorizacion()))) &&
            ((this.codigoCategoriaComercio==null && other.getCodigoCategoriaComercio()==null) || 
             (this.codigoCategoriaComercio!=null &&
              this.codigoCategoriaComercio.equals(other.getCodigoCategoriaComercio()))) &&
            ((this.codigoMoneda==null && other.getCodigoMoneda()==null) || 
             (this.codigoMoneda!=null &&
              this.codigoMoneda.equals(other.getCodigoMoneda()))) &&
            ((this.cuota==null && other.getCuota()==null) || 
             (this.cuota!=null &&
              this.cuota.equals(other.getCuota()))) &&
            ((this.descripcionMovimiento==null && other.getDescripcionMovimiento()==null) || 
             (this.descripcionMovimiento!=null &&
              this.descripcionMovimiento.equals(other.getDescripcionMovimiento()))) &&
            ((this.diasFaltantes==null && other.getDiasFaltantes()==null) || 
             (this.diasFaltantes!=null &&
              this.diasFaltantes.equals(other.getDiasFaltantes()))) &&
            ((this.fechaOrigen==null && other.getFechaOrigen()==null) || 
             (this.fechaOrigen!=null &&
              this.fechaOrigen.equals(other.getFechaOrigen()))) &&
            ((this.fechaPosteo==null && other.getFechaPosteo()==null) || 
             (this.fechaPosteo!=null &&
              this.fechaPosteo.equals(other.getFechaPosteo()))) &&
            ((this.flagFacturado==null && other.getFlagFacturado()==null) || 
             (this.flagFacturado!=null &&
              this.flagFacturado.equals(other.getFlagFacturado()))) &&
            ((this.idMovimientoTransaccion==null && other.getIdMovimientoTransaccion()==null) || 
             (this.idMovimientoTransaccion!=null &&
              this.idMovimientoTransaccion.equals(other.getIdMovimientoTransaccion()))) &&
            ((this.importe==null && other.getImporte()==null) || 
             (this.importe!=null &&
              this.importe.equals(other.getImporte()))) &&
            ((this.numeroTarjeta==null && other.getNumeroTarjeta()==null) || 
             (this.numeroTarjeta!=null &&
              this.numeroTarjeta.equals(other.getNumeroTarjeta()))) &&
            ((this.tipoMovimiento==null && other.getTipoMovimiento()==null) || 
             (this.tipoMovimiento!=null &&
              this.tipoMovimiento.equals(other.getTipoMovimiento()))) &&
            ((this.tipoRelacionFinanciera==null && other.getTipoRelacionFinanciera()==null) || 
             (this.tipoRelacionFinanciera!=null &&
              this.tipoRelacionFinanciera.equals(other.getTipoRelacionFinanciera())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCantidadPuntos() != null) {
            _hashCode += getCantidadPuntos().hashCode();
        }
        if (getCodCatBalance() != null) {
            _hashCode += getCodCatBalance().hashCode();
        }
        if (getCodCatEECC() != null) {
            _hashCode += getCodCatEECC().hashCode();
        }
        if (getCodCatFin() != null) {
            _hashCode += getCodCatFin().hashCode();
        }
        if (getCodProgramaFidelidad() != null) {
            _hashCode += getCodProgramaFidelidad().hashCode();
        }
        if (getCodigoAutorizacion() != null) {
            _hashCode += getCodigoAutorizacion().hashCode();
        }
        if (getCodigoCategoriaComercio() != null) {
            _hashCode += getCodigoCategoriaComercio().hashCode();
        }
        if (getCodigoMoneda() != null) {
            _hashCode += getCodigoMoneda().hashCode();
        }
        if (getCuota() != null) {
            _hashCode += getCuota().hashCode();
        }
        if (getDescripcionMovimiento() != null) {
            _hashCode += getDescripcionMovimiento().hashCode();
        }
        if (getDiasFaltantes() != null) {
            _hashCode += getDiasFaltantes().hashCode();
        }
        if (getFechaOrigen() != null) {
            _hashCode += getFechaOrigen().hashCode();
        }
        if (getFechaPosteo() != null) {
            _hashCode += getFechaPosteo().hashCode();
        }
        if (getFlagFacturado() != null) {
            _hashCode += getFlagFacturado().hashCode();
        }
        if (getIdMovimientoTransaccion() != null) {
            _hashCode += getIdMovimientoTransaccion().hashCode();
        }
        if (getImporte() != null) {
            _hashCode += getImporte().hashCode();
        }
        if (getNumeroTarjeta() != null) {
            _hashCode += getNumeroTarjeta().hashCode();
        }
        if (getTipoMovimiento() != null) {
            _hashCode += getTipoMovimiento().hashCode();
        }
        if (getTipoRelacionFinanciera() != null) {
            _hashCode += getTipoRelacionFinanciera().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MovTrxInfoDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "MovTrxInfoDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cantidadPuntos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "CantidadPuntos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCatBalance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "CodCatBalance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCatEECC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "CodCatEECC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCatFin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "CodCatFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codProgramaFidelidad");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "CodProgramaFidelidad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoAutorizacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "CodigoAutorizacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoCategoriaComercio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "CodigoCategoriaComercio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoMoneda");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "CodigoMoneda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cuota");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "Cuota"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcionMovimiento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "DescripcionMovimiento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diasFaltantes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "DiasFaltantes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaOrigen");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "FechaOrigen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaPosteo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "FechaPosteo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flagFacturado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "FlagFacturado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idMovimientoTransaccion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "IdMovimientoTransaccion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("importe");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "Importe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroTarjeta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "NumeroTarjeta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoMovimiento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "TipoMovimiento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRelacionFinanciera");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "TipoRelacionFinanciera"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
