/**
 * RespuestaConsultaSaldoDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance;

public class RespuestaConsultaSaldoDTO  extends org.datacontract.schemas._2004._07.PS_CommonCredit_ClientWS_Contract_Datatypes.ResponseDTO  implements java.io.Serializable {
    private ConsultaSaldoInfoDTO consultaSaldoInfo;

    private CabeceraInfoDTO header;

    public RespuestaConsultaSaldoDTO() {
    }

    public RespuestaConsultaSaldoDTO(
           org.datacontract.schemas._2004._07.PS_CommonCredit_ClientWS_Contract_Datatypes.ErrorDTO[] errors,
           org.datacontract.schemas._2004._07.PS_CommonCredit_ClientWS_Contract_Enumerations.ReturnExecution returnExecution,
           ConsultaSaldoInfoDTO consultaSaldoInfo,
           CabeceraInfoDTO header) {
        super(
            errors,
            returnExecution);
        this.consultaSaldoInfo = consultaSaldoInfo;
        this.header = header;
    }


    /**
     * Gets the consultaSaldoInfo value for this RespuestaConsultaSaldoDTO.
     * 
     * @return consultaSaldoInfo
     */
    public ConsultaSaldoInfoDTO getConsultaSaldoInfo() {
        return consultaSaldoInfo;
    }


    /**
     * Sets the consultaSaldoInfo value for this RespuestaConsultaSaldoDTO.
     * 
     * @param consultaSaldoInfo
     */
    public void setConsultaSaldoInfo(ConsultaSaldoInfoDTO consultaSaldoInfo) {
        this.consultaSaldoInfo = consultaSaldoInfo;
    }


    /**
     * Gets the header value for this RespuestaConsultaSaldoDTO.
     * 
     * @return header
     */
    public CabeceraInfoDTO getHeader() {
        return header;
    }


    /**
     * Sets the header value for this RespuestaConsultaSaldoDTO.
     * 
     * @param header
     */
    public void setHeader(CabeceraInfoDTO header) {
        this.header = header;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof RespuestaConsultaSaldoDTO)) return false;
        RespuestaConsultaSaldoDTO other = (RespuestaConsultaSaldoDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.consultaSaldoInfo==null && other.getConsultaSaldoInfo()==null) || 
             (this.consultaSaldoInfo!=null &&
              this.consultaSaldoInfo.equals(other.getConsultaSaldoInfo()))) &&
            ((this.header==null && other.getHeader()==null) || 
             (this.header!=null &&
              this.header.equals(other.getHeader())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConsultaSaldoInfo() != null) {
            _hashCode += getConsultaSaldoInfo().hashCode();
        }
        if (getHeader() != null) {
            _hashCode += getHeader().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RespuestaConsultaSaldoDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "RespuestaConsultaSaldoDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consultaSaldoInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "ConsultaSaldoInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "ConsultaSaldoInfoDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("header");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "Header"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CabeceraInfoDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
