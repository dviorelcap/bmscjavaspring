/**
 * CuotasDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance;

public class CuotasDTO  implements java.io.Serializable {
    private String importeAcumCtas;

    private Integer nroCtasPactads;

    private Integer nroCuotaActual;

    public CuotasDTO() {
    }

    public CuotasDTO(
           String importeAcumCtas,
           Integer nroCtasPactads,
           Integer nroCuotaActual) {
           this.importeAcumCtas = importeAcumCtas;
           this.nroCtasPactads = nroCtasPactads;
           this.nroCuotaActual = nroCuotaActual;
    }


    /**
     * Gets the importeAcumCtas value for this CuotasDTO.
     * 
     * @return importeAcumCtas
     */
    public String getImporteAcumCtas() {
        return importeAcumCtas;
    }


    /**
     * Sets the importeAcumCtas value for this CuotasDTO.
     * 
     * @param importeAcumCtas
     */
    public void setImporteAcumCtas(String importeAcumCtas) {
        this.importeAcumCtas = importeAcumCtas;
    }


    /**
     * Gets the nroCtasPactads value for this CuotasDTO.
     * 
     * @return nroCtasPactads
     */
    public Integer getNroCtasPactads() {
        return nroCtasPactads;
    }


    /**
     * Sets the nroCtasPactads value for this CuotasDTO.
     * 
     * @param nroCtasPactads
     */
    public void setNroCtasPactads(Integer nroCtasPactads) {
        this.nroCtasPactads = nroCtasPactads;
    }


    /**
     * Gets the nroCuotaActual value for this CuotasDTO.
     * 
     * @return nroCuotaActual
     */
    public Integer getNroCuotaActual() {
        return nroCuotaActual;
    }


    /**
     * Sets the nroCuotaActual value for this CuotasDTO.
     * 
     * @param nroCuotaActual
     */
    public void setNroCuotaActual(Integer nroCuotaActual) {
        this.nroCuotaActual = nroCuotaActual;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof CuotasDTO)) return false;
        CuotasDTO other = (CuotasDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.importeAcumCtas==null && other.getImporteAcumCtas()==null) || 
             (this.importeAcumCtas!=null &&
              this.importeAcumCtas.equals(other.getImporteAcumCtas()))) &&
            ((this.nroCtasPactads==null && other.getNroCtasPactads()==null) || 
             (this.nroCtasPactads!=null &&
              this.nroCtasPactads.equals(other.getNroCtasPactads()))) &&
            ((this.nroCuotaActual==null && other.getNroCuotaActual()==null) || 
             (this.nroCuotaActual!=null &&
              this.nroCuotaActual.equals(other.getNroCuotaActual())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getImporteAcumCtas() != null) {
            _hashCode += getImporteAcumCtas().hashCode();
        }
        if (getNroCtasPactads() != null) {
            _hashCode += getNroCtasPactads().hashCode();
        }
        if (getNroCuotaActual() != null) {
            _hashCode += getNroCuotaActual().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CuotasDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CuotasDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("importeAcumCtas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "ImporteAcumCtas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroCtasPactads");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "NroCtasPactads"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroCuotaActual");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "NroCuotaActual"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
