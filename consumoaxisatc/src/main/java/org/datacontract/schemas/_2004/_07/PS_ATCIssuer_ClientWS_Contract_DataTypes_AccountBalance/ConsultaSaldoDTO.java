/**
 * ConsultaSaldoDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_AccountBalance;

public class ConsultaSaldoDTO  implements java.io.Serializable {
    private String codigoBanco;

    private String codigoMatriculaSolicitante;

    private String codigoTerminalCanal;

    private String codigoUsuarioAplicativo;

    private String numeroOperacion;

    private String numeroTarjeta;

    public ConsultaSaldoDTO() {
    }

    public ConsultaSaldoDTO(
           String codigoBanco,
           String codigoMatriculaSolicitante,
           String codigoTerminalCanal,
           String codigoUsuarioAplicativo,
           String numeroOperacion,
           String numeroTarjeta) {
           this.codigoBanco = codigoBanco;
           this.codigoMatriculaSolicitante = codigoMatriculaSolicitante;
           this.codigoTerminalCanal = codigoTerminalCanal;
           this.codigoUsuarioAplicativo = codigoUsuarioAplicativo;
           this.numeroOperacion = numeroOperacion;
           this.numeroTarjeta = numeroTarjeta;
    }


    /**
     * Gets the codigoBanco value for this ConsultaSaldoDTO.
     * 
     * @return codigoBanco
     */
    public String getCodigoBanco() {
        return codigoBanco;
    }


    /**
     * Sets the codigoBanco value for this ConsultaSaldoDTO.
     * 
     * @param codigoBanco
     */
    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }


    /**
     * Gets the codigoMatriculaSolicitante value for this ConsultaSaldoDTO.
     * 
     * @return codigoMatriculaSolicitante
     */
    public String getCodigoMatriculaSolicitante() {
        return codigoMatriculaSolicitante;
    }


    /**
     * Sets the codigoMatriculaSolicitante value for this ConsultaSaldoDTO.
     * 
     * @param codigoMatriculaSolicitante
     */
    public void setCodigoMatriculaSolicitante(String codigoMatriculaSolicitante) {
        this.codigoMatriculaSolicitante = codigoMatriculaSolicitante;
    }


    /**
     * Gets the codigoTerminalCanal value for this ConsultaSaldoDTO.
     * 
     * @return codigoTerminalCanal
     */
    public String getCodigoTerminalCanal() {
        return codigoTerminalCanal;
    }


    /**
     * Sets the codigoTerminalCanal value for this ConsultaSaldoDTO.
     * 
     * @param codigoTerminalCanal
     */
    public void setCodigoTerminalCanal(String codigoTerminalCanal) {
        this.codigoTerminalCanal = codigoTerminalCanal;
    }


    /**
     * Gets the codigoUsuarioAplicativo value for this ConsultaSaldoDTO.
     * 
     * @return codigoUsuarioAplicativo
     */
    public String getCodigoUsuarioAplicativo() {
        return codigoUsuarioAplicativo;
    }


    /**
     * Sets the codigoUsuarioAplicativo value for this ConsultaSaldoDTO.
     * 
     * @param codigoUsuarioAplicativo
     */
    public void setCodigoUsuarioAplicativo(String codigoUsuarioAplicativo) {
        this.codigoUsuarioAplicativo = codigoUsuarioAplicativo;
    }


    /**
     * Gets the numeroOperacion value for this ConsultaSaldoDTO.
     * 
     * @return numeroOperacion
     */
    public String getNumeroOperacion() {
        return numeroOperacion;
    }


    /**
     * Sets the numeroOperacion value for this ConsultaSaldoDTO.
     * 
     * @param numeroOperacion
     */
    public void setNumeroOperacion(String numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }


    /**
     * Gets the numeroTarjeta value for this ConsultaSaldoDTO.
     * 
     * @return numeroTarjeta
     */
    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }


    /**
     * Sets the numeroTarjeta value for this ConsultaSaldoDTO.
     * 
     * @param numeroTarjeta
     */
    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof ConsultaSaldoDTO)) return false;
        ConsultaSaldoDTO other = (ConsultaSaldoDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codigoBanco==null && other.getCodigoBanco()==null) || 
             (this.codigoBanco!=null &&
              this.codigoBanco.equals(other.getCodigoBanco()))) &&
            ((this.codigoMatriculaSolicitante==null && other.getCodigoMatriculaSolicitante()==null) || 
             (this.codigoMatriculaSolicitante!=null &&
              this.codigoMatriculaSolicitante.equals(other.getCodigoMatriculaSolicitante()))) &&
            ((this.codigoTerminalCanal==null && other.getCodigoTerminalCanal()==null) || 
             (this.codigoTerminalCanal!=null &&
              this.codigoTerminalCanal.equals(other.getCodigoTerminalCanal()))) &&
            ((this.codigoUsuarioAplicativo==null && other.getCodigoUsuarioAplicativo()==null) || 
             (this.codigoUsuarioAplicativo!=null &&
              this.codigoUsuarioAplicativo.equals(other.getCodigoUsuarioAplicativo()))) &&
            ((this.numeroOperacion==null && other.getNumeroOperacion()==null) || 
             (this.numeroOperacion!=null &&
              this.numeroOperacion.equals(other.getNumeroOperacion()))) &&
            ((this.numeroTarjeta==null && other.getNumeroTarjeta()==null) || 
             (this.numeroTarjeta!=null &&
              this.numeroTarjeta.equals(other.getNumeroTarjeta())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodigoBanco() != null) {
            _hashCode += getCodigoBanco().hashCode();
        }
        if (getCodigoMatriculaSolicitante() != null) {
            _hashCode += getCodigoMatriculaSolicitante().hashCode();
        }
        if (getCodigoTerminalCanal() != null) {
            _hashCode += getCodigoTerminalCanal().hashCode();
        }
        if (getCodigoUsuarioAplicativo() != null) {
            _hashCode += getCodigoUsuarioAplicativo().hashCode();
        }
        if (getNumeroOperacion() != null) {
            _hashCode += getNumeroOperacion().hashCode();
        }
        if (getNumeroTarjeta() != null) {
            _hashCode += getNumeroTarjeta().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConsultaSaldoDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "ConsultaSaldoDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoBanco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodigoBanco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoMatriculaSolicitante");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodigoMatriculaSolicitante"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoTerminalCanal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodigoTerminalCanal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoUsuarioAplicativo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "CodigoUsuarioAplicativo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroOperacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "NumeroOperacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroTarjeta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.AccountBalance", "NumeroTarjeta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
