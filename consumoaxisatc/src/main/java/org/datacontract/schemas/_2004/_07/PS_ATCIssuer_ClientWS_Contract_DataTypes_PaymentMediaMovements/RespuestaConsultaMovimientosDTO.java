/**
 * RespuestaConsultaMovimientosDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.PS_ATCIssuer_ClientWS_Contract_DataTypes_PaymentMediaMovements;

public class RespuestaConsultaMovimientosDTO  extends org.datacontract.schemas._2004._07.PS_CommonCredit_ClientWS_Contract_Datatypes.ResponseDTO  implements java.io.Serializable {
    private MovTrxInfoDTO[] consultaMovimientosInfo;

    private CabeceraInfoDTO header;

    public RespuestaConsultaMovimientosDTO() {
    }

    public RespuestaConsultaMovimientosDTO(
           org.datacontract.schemas._2004._07.PS_CommonCredit_ClientWS_Contract_Datatypes.ErrorDTO[] errors,
           org.datacontract.schemas._2004._07.PS_CommonCredit_ClientWS_Contract_Enumerations.ReturnExecution returnExecution,
           MovTrxInfoDTO[] consultaMovimientosInfo,
           CabeceraInfoDTO header) {
        super(
            errors,
            returnExecution);
        this.consultaMovimientosInfo = consultaMovimientosInfo;
        this.header = header;
    }


    /**
     * Gets the consultaMovimientosInfo value for this RespuestaConsultaMovimientosDTO.
     * 
     * @return consultaMovimientosInfo
     */
    public MovTrxInfoDTO[] getConsultaMovimientosInfo() {
        return consultaMovimientosInfo;
    }


    /**
     * Sets the consultaMovimientosInfo value for this RespuestaConsultaMovimientosDTO.
     * 
     * @param consultaMovimientosInfo
     */
    public void setConsultaMovimientosInfo(MovTrxInfoDTO[] consultaMovimientosInfo) {
        this.consultaMovimientosInfo = consultaMovimientosInfo;
    }


    /**
     * Gets the header value for this RespuestaConsultaMovimientosDTO.
     * 
     * @return header
     */
    public CabeceraInfoDTO getHeader() {
        return header;
    }


    /**
     * Sets the header value for this RespuestaConsultaMovimientosDTO.
     * 
     * @param header
     */
    public void setHeader(CabeceraInfoDTO header) {
        this.header = header;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof RespuestaConsultaMovimientosDTO)) return false;
        RespuestaConsultaMovimientosDTO other = (RespuestaConsultaMovimientosDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.consultaMovimientosInfo==null && other.getConsultaMovimientosInfo()==null) || 
             (this.consultaMovimientosInfo!=null &&
              java.util.Arrays.equals(this.consultaMovimientosInfo, other.getConsultaMovimientosInfo()))) &&
            ((this.header==null && other.getHeader()==null) || 
             (this.header!=null &&
              this.header.equals(other.getHeader())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConsultaMovimientosInfo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConsultaMovimientosInfo());
                 i++) {
                Object obj = java.lang.reflect.Array.get(getConsultaMovimientosInfo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getHeader() != null) {
            _hashCode += getHeader().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RespuestaConsultaMovimientosDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "RespuestaConsultaMovimientosDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consultaMovimientosInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "ConsultaMovimientosInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "MovTrxInfoDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "MovTrxInfoDTO"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("header");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "Header"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.ATCIssuer.ClientWS.Contract.DataTypes.PaymentMediaMovements", "CabeceraInfoDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
