/**
 * ResponseDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.PS_CommonCredit_ClientWS_Contract_Datatypes;

public class ResponseDTO  implements java.io.Serializable {
    private ErrorDTO[] errors;

    private org.datacontract.schemas._2004._07.PS_CommonCredit_ClientWS_Contract_Enumerations.ReturnExecution returnExecution;

    public ResponseDTO() {
    }

    public ResponseDTO(
           ErrorDTO[] errors,
           org.datacontract.schemas._2004._07.PS_CommonCredit_ClientWS_Contract_Enumerations.ReturnExecution returnExecution) {
           this.errors = errors;
           this.returnExecution = returnExecution;
    }


    /**
     * Gets the errors value for this ResponseDTO.
     * 
     * @return errors
     */
    public ErrorDTO[] getErrors() {
        return errors;
    }


    /**
     * Sets the errors value for this ResponseDTO.
     * 
     * @param errors
     */
    public void setErrors(ErrorDTO[] errors) {
        this.errors = errors;
    }


    /**
     * Gets the returnExecution value for this ResponseDTO.
     * 
     * @return returnExecution
     */
    public org.datacontract.schemas._2004._07.PS_CommonCredit_ClientWS_Contract_Enumerations.ReturnExecution getReturnExecution() {
        return returnExecution;
    }


    /**
     * Sets the returnExecution value for this ResponseDTO.
     * 
     * @param returnExecution
     */
    public void setReturnExecution(org.datacontract.schemas._2004._07.PS_CommonCredit_ClientWS_Contract_Enumerations.ReturnExecution returnExecution) {
        this.returnExecution = returnExecution;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof ResponseDTO)) return false;
        ResponseDTO other = (ResponseDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.errors==null && other.getErrors()==null) || 
             (this.errors!=null &&
              java.util.Arrays.equals(this.errors, other.getErrors()))) &&
            ((this.returnExecution==null && other.getReturnExecution()==null) || 
             (this.returnExecution!=null &&
              this.returnExecution.equals(other.getReturnExecution())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErrors() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getErrors());
                 i++) {
                Object obj = java.lang.reflect.Array.get(getErrors(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getReturnExecution() != null) {
            _hashCode += getReturnExecution().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResponseDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.CommonCredit.ClientWS.Contract.Datatypes", "ResponseDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errors");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.CommonCredit.ClientWS.Contract.Datatypes", "Errors"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.CommonCredit.ClientWS.Contract.Datatypes", "ErrorDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.CommonCredit.ClientWS.Contract.Datatypes", "ErrorDTO"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnExecution");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.CommonCredit.ClientWS.Contract.Datatypes", "ReturnExecution"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/PS.CommonCredit.ClientWS.Contract.Enumerations", "ReturnExecution"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
