/**
 * PaymentMediaMovementsServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class PaymentMediaMovementsServiceLocator extends org.apache.axis.client.Service implements PaymentMediaMovementsService {

    public PaymentMediaMovementsServiceLocator() {
    }


    public PaymentMediaMovementsServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PaymentMediaMovementsServiceLocator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BasicHttpBinding_IPaymentMediaMovementsService
    private String BasicHttpBinding_IPaymentMediaMovementsService_address = "https://10.27.115.50:41733/ATC_WebServiceBMSC/ATCIssuer.ClientWS.ServiceHost/Services/PaymentMediaMovementsService.svc";

    public String getBasicHttpBinding_IPaymentMediaMovementsServiceAddress() {
        return BasicHttpBinding_IPaymentMediaMovementsService_address;
    }

    // The WSDD service name defaults to the port name.
    private String BasicHttpBinding_IPaymentMediaMovementsServiceWSDDServiceName = "BasicHttpBinding_IPaymentMediaMovementsService";

    public String getBasicHttpBinding_IPaymentMediaMovementsServiceWSDDServiceName() {
        return BasicHttpBinding_IPaymentMediaMovementsServiceWSDDServiceName;
    }

    public void setBasicHttpBinding_IPaymentMediaMovementsServiceWSDDServiceName(String name) {
        BasicHttpBinding_IPaymentMediaMovementsServiceWSDDServiceName = name;
    }

    public IPaymentMediaMovementsService getBasicHttpBinding_IPaymentMediaMovementsService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BasicHttpBinding_IPaymentMediaMovementsService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBasicHttpBinding_IPaymentMediaMovementsService(endpoint);
    }

    public IPaymentMediaMovementsService getBasicHttpBinding_IPaymentMediaMovementsService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            BasicHttpBinding_IPaymentMediaMovementsServiceStub _stub = new BasicHttpBinding_IPaymentMediaMovementsServiceStub(portAddress, this);
            _stub.setPortName(getBasicHttpBinding_IPaymentMediaMovementsServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBasicHttpBinding_IPaymentMediaMovementsServiceEndpointAddress(String address) {
        BasicHttpBinding_IPaymentMediaMovementsService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (IPaymentMediaMovementsService.class.isAssignableFrom(serviceEndpointInterface)) {
                BasicHttpBinding_IPaymentMediaMovementsServiceStub _stub = new BasicHttpBinding_IPaymentMediaMovementsServiceStub(new java.net.URL(BasicHttpBinding_IPaymentMediaMovementsService_address), this);
                _stub.setPortName(getBasicHttpBinding_IPaymentMediaMovementsServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("BasicHttpBinding_IPaymentMediaMovementsService".equals(inputPortName)) {
            return getBasicHttpBinding_IPaymentMediaMovementsService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "PaymentMediaMovementsService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "BasicHttpBinding_IPaymentMediaMovementsService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException {
        
if ("BasicHttpBinding_IPaymentMediaMovementsService".equals(portName)) {
            setBasicHttpBinding_IPaymentMediaMovementsServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
