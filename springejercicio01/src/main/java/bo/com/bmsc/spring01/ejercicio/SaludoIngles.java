package bo.com.bmsc.spring01.ejercicio;

import org.springframework.stereotype.Component;

@Component("en")
public class SaludoIngles implements Saludo {
    @Override
    public String saludar() {
        return "hello";
    }
}
