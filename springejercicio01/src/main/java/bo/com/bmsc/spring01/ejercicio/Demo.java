package bo.com.bmsc.spring01.ejercicio;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Scanner;

public class Demo {

    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("ApplicationContext.xml");
        System.out.println("Ingrese un idioma (es, en, fr)");
        Scanner scanner = new Scanner(System.in);
        String idioma = scanner.next();
        Saludo saludo = (Saludo) context.getBean(idioma);
        System.out.println(saludo.saludar());
    }

}
