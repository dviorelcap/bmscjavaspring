package bo.com.bmsc.spring01.ejercicio;

import org.springframework.stereotype.Component;

@Component("fr")
public class SaludoFrances implements Saludo {
    @Override
    public String saludar() {
        return "bonjour";
    }
}
