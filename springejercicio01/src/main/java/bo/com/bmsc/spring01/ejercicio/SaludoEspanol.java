package bo.com.bmsc.spring01.ejercicio;

import org.springframework.stereotype.Component;

@Component
public class SaludoEspanol implements Saludo {
    @Override
    public String saludar() {
        return "hola";
    }
}
