package bo.com.bmsc.spring01;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo {

    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("ApplicationContext.xml");
        // Bean1 bean1 = new Bean1();
        // Bean2 bean2 = new Bean2();
        int x = 0;
        Bean1 bean1 = (Bean1) context.getBean("idBean1");
        Bean2 bean2 = (Bean2) context.getBean("idBean2");
        Bean3 bean3 = (Bean3) context.getBean("bean3");
        Bean1 bean4 = (Bean1) context.getBean("idBean1");
        Bean1 bean5 = (Bean1) context.getBean("idBean1");
        Bean1 bean6 = (Bean1) context.getBean("idBean1");
//        bean1.saludar();
//        bean2.saludar();
        bean3.saludar();
    }

}
